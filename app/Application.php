<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace ts\catalog;

use Yii;
use ts\catalog\models\ProductReader;

class Application extends \yii\web\Application
{
    public function getProductIdSlug(ProductReader $reader): ?string
    {
        $params = explode("|", $reader->readerParam);
        $productIdSlug = null;
        foreach ($params as $param) {
            $productIdSlug = Yii::$app->request->get(
                $param,
                get_query_var($param)
            );

            if (!empty($productIdSlug) && strlen((string) $productIdSlug) > 1) {
                break;
            }
        }
        return $productIdSlug ? (string) $productIdSlug : null;
    }
}
