<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace ts\catalog;

class CatalogAssetBundle extends \yii\web\AssetBundle
{
    public $depends = [YiiAsset::class];

    public $sourcePath = '@ts/catalog/asset-sources';

    public $js = [
        'js/AdminPanel.js'
    ];
}
