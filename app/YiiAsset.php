<?php

namespace ts\catalog;

class YiiAsset extends \yii\web\YiiAsset implements interfaces\WordpressAssetAwareInterface
{
    public $depends = [];

    public function getWordpressScriptDeps(): array
    {
        return ['jquery'];
    }

    public function getWordpressStyleDeps(): array
    {
        return [];
    }
}
