<?php

declare(strict_types=1);

namespace ts\catalog\components;

use Yii;
use yii\httpclient\Request as YiiRequest;

class Request extends YiiRequest
{
    /**
     * No ucwords for header values here!
     */
    public function composeHeaderLines()
    {
        if (!$this->hasHeaders()) {
            return [];
        }
        $headers = [];
        foreach ($this->getHeaders() as $name => $values) {
            $name = str_replace(' ', '-', str_replace('-', ' ', $name));
            foreach ($values as $value) {
                $headers[] = "$name: $value";
            }
        }

        if ($this->hasCookies()) {
            $headers[] = $this->composeCookieHeader();
        }

        return $headers;
    }
}
