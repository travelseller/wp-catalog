<?php

declare(strict_types=1);

namespace ts\catalog\components;

use Yii;
use ts\catalog\interfaces\WordpressAssetAwareInterface;

/**
 * register assets to wordpress
 */
class View extends \yii\web\View
{
    public function registerAssetsToWp()
    {
        foreach (array_keys($this->assetBundles) as $bundle) {
            $this->registerAssetFilesToWp($bundle);
        }
    }

    protected function registerAssetFilesToWp($name)
    {
        if (!isset($this->assetBundles[$name])) {
            return;
        }
        $bundle = $this->assetBundles[$name];
        if ($bundle) {
            foreach ($bundle->depends as $dep) {
                $this->registerAssetFilesToWp($dep);
            }

            foreach ($bundle->js as $script) {
                $scriptDeps = [];
                if ($bundle instanceof WordpressAssetAwareInterface) {
                    $scriptDeps = $bundle->getWordpressScriptDeps();
                }
                wp_enqueue_script($script, $bundle->baseUrl . '/' . $script, $scriptDeps, false, true);
            }

            foreach ($bundle->css as $style) {
                $styleDeps = [];
                if ($bundle instanceof WordpressAssetAwareInterface) {
                    $styleDeps = $bundle->getWordpressStyleDeps();
                }
                wp_enqueue_style($style, $bundle->baseUrl . '/' . $style, $styleDeps);
            }
        }

        unset($this->assetBundles[$name]);
    }
}
