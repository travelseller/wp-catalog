<?php

declare(strict_types=1);

namespace ts\catalog\console;

use Yii;

/**
 * create a Build from source: Removes all vcs-folders and test-folders.
 * tar must be available!
 */
class BuildController extends \yii\console\Controller
{
    public function actionIndex($of=null)
    {
        $if = dirname(Yii::getAlias('@app'));

        if (!$of) {
            $of = dirname(Yii::getAlias('@app'), 2);
        }

        $archivename = 'travelseller-catalog-build.tar.gz';

        if (file_exists($of . '/' . $archivename)) {
            unlink($of . '/' . $archivename);
        }

        exec("tar -C " . dirname($if) . " --exclude={$archivename} --exclude=.git* --exclude=tests --exclude=.gitignore --exclude 'app/assets/*' --exclude 'app/views/customs/*' -czf {$of}/{$archivename} " . basename($if) . "/");

        echo "Build generated: {$of}/{$archivename}\n";
    }
}
