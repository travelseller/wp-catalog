<?php

declare(strict_types=1);

namespace ts\catalog\controllers;

use Yii;
use ts\catalog\models as Models;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DebugController extends \yii\web\Controller
{
    public function actionDownload()
    {
        $debugFile = ABSPATH . '/wp-content/debug.log';

        if (!file_exists($debugFile)) {
            return;
        }

        Yii::$app->response->sendFile($debugFile, 'debug.log')->send();
    }
}
