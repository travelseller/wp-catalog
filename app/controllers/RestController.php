<?php

declare(strict_types=1);

namespace ts\catalog\controllers;

use Yii;
use ts\catalog\models as Models;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class RestController extends \yii\web\Controller
{
    /**
     * @var Models\Config
     */
    private $config;

    public function actionIndex(): Response
    {
        $config = Yii::$app->request->get('config');
        $endpoint = Yii::$app->request->get('endpoint');

        $this->config = Models\Config::find()->where(['pid' => $config])->one();
        if (!$this->config) {
            throw new NotFoundHttpException("config-id missing or not found! Try to provide a Catalog-Config-Id as GET-query-param 'config'");
        }

        switch ($endpoint) {
            case "lister":
                return $this->lister();
                break;
            case "reader":
                return $this->reader();
                break;
            default:
                throw new \Exception("unknown endpoint");
        }
    }

    protected function lister(): Response
    {
        $lister = $this->config->lister;
        $filterKey = $lister->filter->filterKey ?? $lister->formName();
        $params = array_merge(
            Yii::$app->request->get($filterKey, []),
            Yii::$app->request->post($filterKey, [])
        );
        $dp = $lister->filter->search($params);

        Yii::$app->response->data = $dp->allModels;

        return Yii::$app->response;
    }

    protected function reader(): Response
    {
        if (!($productId = Yii::$app->getProductIdSlug($this->config->reader))) {
            throw new NotFoundHttpException("readerParam missing or product not found!");
        }
        Yii::$app->response->data = $this->config->endpoint->findOne((int) $productId);

        return Yii::$app->response;
    }
}
