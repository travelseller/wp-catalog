<?php

namespace ts\catalog\interfaces;

use ts\catalog\models\Connection;

interface EndpointInterface
{
    public static function getInstance(Connection $connection): EndpointInterface;

    /**
     * query all Items
     * This should return a "short" Version of Items
     * suitable for List-View.
     *
     * @return EndpointInterface[]
     */
    public function findAll(array $filter=[]): array;

    /**
     * query a single item
     * This should return a "long" Version of a Item
     * suitable for a Detail-View
     *
     * @return EndpointInterface
     */
    public function findOne(int $id): ?EndpointInterface;

    /**
     * The Primary Key to unique ident. a item
     */
    public function getPrimaryKey();

    /**
     * the product price per unit
     * @param array $options optional Productoptions
     */
    public function getPrice(array $options=[]): ?float;

    /**
     * returns the raw-data for a hydrated item
     */
    public function getRaw(): array;

    /**
     * hydrates the Model (becomes an entity).
     * "hydrate" means, we add $rawData to the models propertys.
     * A implementation could something like:
     *
     * public function hydrate(array $rawData)
     * {
     *   foreach ($rawData as $attr => $value) {
     *      $this->{$attr} = $value;
     *   }
     *   $this->raw = $rawData;
     * }
     */
    public function hydrate(array $rawData);
}
