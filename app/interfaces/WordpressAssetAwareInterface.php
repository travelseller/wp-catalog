<?php

namespace ts\catalog\interfaces;

interface WordpressAssetAwareInterface
{
    public function getWordpressScriptDeps(): array;

    public function getWordpressStyleDeps(): array;
}
