<?php

declare(strict_types=1);

namespace ts\catalog\interfaces;

use Yii;

interface WpPostInterface
{
    /**
     * the Posttype this AR correspondece to
     */
    public static function getWpType(): string;
}
