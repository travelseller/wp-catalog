<?php

use yii\db\Migration;
use app\helpers\MigrationHelper;

/**
 * Class m191119_103515_createTables
 */
class m191119_103515_createTables extends \yii\db\Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tsibe_connections}}', array_merge(MigrationHelper::getSystemCols(), [
            'modelClass' => $this->text(),
            'endpoint_url' => $this->text(),
            'partner_id' => $this->string(10),
            'username' => $this->string(100),
            'password' => $this->string(100)
        ]));

        $this->createTable('{{%tsibe_configs}}', array_merge(MigrationHelper::getSystemCols(), [
            'modelClass' => $this->text(),
            'connection_id' => $this->integer(11),
            'module_template' => $this->text(),
            'item_template' => $this->text(),
            'filterKey' => $this->string(100),
            'showPager' => $this->boolean(),
            'pagerSize' => $this->integer(3),
            'pagerParam' => $this->string(30),
            'pagerContainerCssClasses' => $this->text(),
            'pagerLinkContainerCssClasses' => $this->text(),
            'pagerLinkCssClasses' => $this->text(),
            'pagerLinkDataAttributes' => $this->text(),
            'readerPage' => $this->text(),
            'readerParam' => $this->string(30),
            'filterPage' => $this->text(),
            'defaultFilterParams' => $this->text(),
            'enablePjax' => $this->boolean(),
            'hideWhenReaderActive' => $this->boolean(),
        ]));

        $this->createIndex('idx-configs-connection_id', '{{%tsibe_configs}}', 'connection_id');

        $this->addForeignKey(
            'fk-configs-connection_id',
            '{{%tsibe_configs}}',
            'connection_id',
            '{{%tsibe_connections}}',
            'id',
            'CASCADE'
        );
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-configs-connection_id',
            '{{%tsibe_configs}}'
        );

        $this->dropIndex(
            'idx-configs-connection_id',
            '{{%tsibe_configs}}'
        );

        $this->dropTable('{{%tsibe_connections}}');
        $this->dropTable('{{%tsibe_configs}}');
    }
}
