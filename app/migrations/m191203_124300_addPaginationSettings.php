<?php

use yii\db\Migration;

/**
 * Class m191203_124300_addPaginationSettings
 */
class m191203_124300_addPaginationSettings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'cacheStrategy', $this->string(200));
        $this->addColumn('{{%tsibe_configs}}', 'cacheDuration', $this->integer(11));
        $this->addColumn('{{%tsibe_configs}}', 'redisHost', $this->text());
        $this->addColumn('{{%tsibe_configs}}', 'redisPort', $this->integer(6));
        $this->addColumn('{{%tsibe_configs}}', 'redisDb', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'cacheStrategy');
        $this->dropColumn('{{%tsibe_configs}}', 'cacheDuration');
        $this->dropColumn('{{%tsibe_configs}}', 'redisHost');
        $this->dropColumn('{{%tsibe_configs}}', 'redisPort');
        $this->dropColumn('{{%tsibe_configs}}', 'redisDb');
    }
}
