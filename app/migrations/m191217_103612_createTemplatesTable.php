<?php

use yii\db\Migration;
use app\helpers\MigrationHelper;

/**
 * Handles the creation of table `{{%tsibe_templates}}`.
 */
class m191217_103612_createTemplatesTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tsibe_templates}}', array_merge(MigrationHelper::getSystemCols(), [
            'modelClass' => $this->text(),
        ]));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tsibe_templates}}');
    }
}
