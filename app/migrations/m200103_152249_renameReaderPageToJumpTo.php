<?php

use yii\db\Migration;

/**
 * Class m200103_152249_renameReaderPageToJumpTo
 */
class m200103_152249_renameReaderPageToJumpTo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%tsibe_configs}}', 'readerPage', 'jumpTo');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%tsibe_configs}}', 'jumpTo', 'readerPage');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200103_152249_renameReaderPageToJumpTo cannot be reverted.\n";

        return false;
    }
    */
}
