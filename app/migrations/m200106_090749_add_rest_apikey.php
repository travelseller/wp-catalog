<?php

use yii\db\Migration;

/**
 * Class m200106_090749_add_rest_apikey
 */
class m200106_090749_add_rest_apikey extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_connections}}', 'apiKey', $this->string(255));
        $this->addColumn('{{%tsibe_connections}}', 'brandId', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_connections}}', 'apiKey');
        $this->dropColumn('{{%tsibe_connections}}', 'brandId');
    }
}
