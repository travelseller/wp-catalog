<?php

use yii\db\Migration;

/**
 * Class m200122_092401_addListerContainerDataAttributes
 */
class m200122_092401_addListerContainerDataAttributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'listerContainerDataAttributes', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'listerContainerDataAttributes');
    }
}
