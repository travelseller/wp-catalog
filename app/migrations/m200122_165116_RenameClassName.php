<?php

use yii\db\Migration;
use ts\catalog\models\Config;
use ts\catalog\models\Connection;
use ts\catalog\models\ProductFilter;
use ts\catalog\models\ProductLister;
use ts\catalog\models\ProductReader;
use ts\catalog\models\Template;

/**
 * Class m200122_165116_RenameClassName
 */
class m200122_165116_RenameClassName extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->db->createCommand()->update(Connection::tableName(), [
            'modelClass' => Connection::class
        ], 'modelClass = :class', [
            ':class' => "app\models\Connection"
        ])->execute();

        Yii::$app->db->createCommand()->update(Config::tableName(), [
            'modelClass' => Config::class
        ], 'modelClass = :class', [
            ':class' => "app\models\Config"
        ])->execute();

        Yii::$app->db->createCommand()->update(ProductFilter::tableName(), [
            'modelClass' => ProductFilter::class
        ], 'modelClass = :class', [
            ':class' => "app\models\ProductFilter"
        ])->execute();

        Yii::$app->db->createCommand()->update(ProductLister::tableName(), [
            'modelClass' => ProductLister::class
        ], 'modelClass = :class', [
            ':class' => "app\models\ProductLister"
        ])->execute();

        Yii::$app->db->createCommand()->update(ProductReader::tableName(), [
            'modelClass' => ProductReader::class
        ], 'modelClass = :class', [
            ':class' => "app\models\ProductReader"
        ])->execute();

        Yii::$app->db->createCommand()->update(Template::tableName(), [
            'modelClass' => Template::class
        ], 'modelClass = :class', [
            ':class' => "app\models\Template"
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
