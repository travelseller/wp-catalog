<?php

use yii\db\Migration;

/**
 * Class m200122_190702_AddNavTemplate
 */
class m200122_190702_AddNavTemplate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%tsibe_configs}}', 'module_template', $this->integer(11));
        $this->alterColumn('{{%tsibe_configs}}', 'item_template', $this->integer(11));

        $this->createIndex('idx-configs-module_template', '{{%tsibe_configs}}', 'module_template');
        $this->createIndex('idx-configs-item_template', '{{%tsibe_configs}}', 'item_template');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
