<?php

use yii\db\Migration;

/**
 * Class m200123_094809_AddFilterType
 */
class m200123_094809_AddFilterType extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'nav_filter_type', $this->string(250));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'nav_filter_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200123_094809_AddFilterType cannot be reverted.\n";

        return false;
    }
    */
}
