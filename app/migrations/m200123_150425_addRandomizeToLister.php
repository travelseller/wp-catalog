<?php

use yii\db\Migration;

/**
 * Class m200123_150425_addRandomizeToLister
 */
class m200123_150425_addRandomizeToLister extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'randomize', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'randomize');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200123_150425_addRandomizeToLister cannot be reverted.\n";

        return false;
    }
    */
}
