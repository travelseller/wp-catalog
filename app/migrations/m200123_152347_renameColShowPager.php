<?php

use yii\db\Migration;

/**
 * Class m200123_152347_renameColShowPager
 */
class m200123_152347_renameColShowPager extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%tsibe_configs}}', 'showPager', 'usePager');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%tsibe_configs}}', 'usePager', 'showPager');
    }
}
