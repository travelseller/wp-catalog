<?php

use yii\db\Migration;

/**
 * Class m200317_200932_addModuleCssItemClassesCol
 */
class m200317_200932_addModuleCssItemClassesCol extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'moduleCssItemClasses', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('{{%tsibe_configs}}', 'moduleCssItemClasses');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200317_200932_addModuleCssItemClassesCol cannot be reverted.\n";

        return false;
    }
    */
}
