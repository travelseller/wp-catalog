<?php

use yii\db\Migration;

/**
 * Class m200902_092603_addMenueRenderSettingsBool
 */
class m200902_092603_addMenueRenderSettingsBool extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'nav_lvl1_as_menuitem', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'nav_lvl1_as_menuitem');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_092603_addMenueRenderSettingsBool cannot be reverted.\n";

        return false;
    }
    */
}
