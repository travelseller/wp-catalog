<?php

use yii\db\Migration;

/**
 * Class m210302_104425_add_boxnumber_to_navfilter
 */
class m210302_104425_add_boxnumber_to_navfilter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'nav_boxid_whitelist', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'nav_boxid_whitelist');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210302_104425_add_boxnumber_to_navfilter cannot be reverted.\n";

        return false;
    }
    */
}
