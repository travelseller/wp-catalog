<?php

use yii\db\Migration;

/**
 * Class m210308_162024_addEndpoints
 */
class m210308_162024_addEndpoints extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'endpointType', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'endpointType');
    }
}
