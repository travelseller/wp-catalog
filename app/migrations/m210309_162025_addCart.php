<?php

use yii\db\Migration;

/**
 * Class m210309_162025_addCart.php
 */
class m210309_162025_addCart extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'cartAddedText', $this->text());
        $this->addColumn('{{%tsibe_configs}}', 'cartJumpToAfterAddItem', $this->text());

        $this->createTable('{{%tsibe_carts}}', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer(),
            'settings_id' => $this->integer(),
            'sessId' => $this->text(),
            'product_id' => $this->integer(),
            'productModelClass' => $this->text(),
            'qty' => $this->integer(),
            'unit_price' => $this->decimal(6, 2),
            'product_raw_data' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%tsibe_carts}}');
        $this->dropColumn('{{%tsibe_configs}}', 'cartAddedText');
        $this->dropColumn('{{%tsibe_configs}}', 'cartJumpToAfterAddItem');
    }
}
