<?php

use yii\db\Migration;

/**
 * Class m210311_135952_addCartItemOptions
 */
class m210311_135952_addCartItemOptions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_carts}}', 'options', $this->text());
        $this->addColumn('{{%tsibe_carts}}', 'sku', $this->string(250));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_carts}}', 'options');
        $this->dropColumn('{{%tsibe_carts}}', 'sku');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210311_135952_addCartItemOptions cannot be reverted.\n";

        return false;
    }
    */
}
