<?php

use yii\db\Migration;

/**
 * Class m210312_135953_moveCacheSettingsToConnection
 */
class m210312_135953_moveCacheSettingsToConnection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_connections}}', 'cacheStrategy', $this->string(200));
        $this->addColumn('{{%tsibe_connections}}', 'cacheDuration', $this->integer(11));
        $this->addColumn('{{%tsibe_connections}}', 'redisHost', $this->text());
        $this->addColumn('{{%tsibe_connections}}', 'redisPort', $this->integer(6));
        $this->addColumn('{{%tsibe_connections}}', 'redisDb', $this->text());
        $this->dropColumn('{{%tsibe_configs}}', 'cacheStrategy');
        $this->dropColumn('{{%tsibe_configs}}', 'cacheDuration');
        $this->dropColumn('{{%tsibe_configs}}', 'redisHost');
        $this->dropColumn('{{%tsibe_configs}}', 'redisPort');
        $this->dropColumn('{{%tsibe_configs}}', 'redisDb');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_connections}}', 'cacheStrategy');
        $this->dropColumn('{{%tsibe_connections}}', 'cacheDuration');
        $this->dropColumn('{{%tsibe_connections}}', 'redisHost');
        $this->dropColumn('{{%tsibe_connections}}', 'redisPort');
        $this->dropColumn('{{%tsibe_connections}}', 'redisDb');
        $this->addColumn('{{%tsibe_configs}}', 'cacheStrategy', $this->string(200));
        $this->addColumn('{{%tsibe_configs}}', 'cacheDuration', $this->integer(11));
        $this->addColumn('{{%tsibe_configs}}', 'redisHost', $this->text());
        $this->addColumn('{{%tsibe_configs}}', 'redisPort', $this->integer(6));
        $this->addColumn('{{%tsibe_configs}}', 'redisDb', $this->text());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210311_135952_addCartItemOptions cannot be reverted.\n";

        return false;
    }
    */
}
