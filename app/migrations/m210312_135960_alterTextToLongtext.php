<?php

use yii\db\Migration;

class m210312_135960_alterTextToLongtext extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%tsibe_carts}}', 'product_raw_data', $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%tsibe_carts}}', 'product_raw_data', $this->text());
    }
}
