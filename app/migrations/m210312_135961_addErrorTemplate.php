<?php

use yii\db\Migration;

class m210312_135961_addErrorTemplate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tsibe_configs}}', 'error_template', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tsibe_configs}}', 'error_template');
    }
}
