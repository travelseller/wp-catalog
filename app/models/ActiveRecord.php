<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;

class ActiveRecord extends \yii\db\ActiveRecord
{
    public function beforeSave($insert)
    {
        $this->modelClass = static::class;

        return parent::beforeSave($insert);
    }

    public static function find()
    {
        $finder = parent::find();
        $finder->where(['modelClass' => static::class]);

        return $finder;
    }
}
