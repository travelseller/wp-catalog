<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\FileHelper;
use kartik\detail\DetailView;
use ts\catalog\traits\WordPressTrait;
use ts\catalog\interfaces\WpPostInterface;

class BaseConfig extends ActiveRecord implements WpPostInterface
{
    use WordPressTrait;

    public static function getWpType(): string
    {
        return 'tscatalogconfig';
    }

    public static function tableName()
    {
        return '{{%tsibe_configs}}';
    }

    public function getConfig()
    {
        return $this->hasOne(Config::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => Config::class]);
    }

    public function attributeLabels()
    {
        return [
            'module_template' => Yii::t('tsCatalog', 'Module Template'),
            'item_template' => Yii::t('tsCatalog', 'Item Template'),
        ];
    }
}
