<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use ts\catalog\interfaces\EndpointInterface;

class Cart extends \yii\db\ActiveRecord
{
    public const ACTION_REMOVE = 'removeItem';
    public const ACTION_ADD = 'addItem';

    public static function tableName()
    {
        return '{{%tsibe_carts}}';
    }

    public function rules()
    {
        return [
            [['sessId'],'required']
        ];
    }

    public function getCartWorth(array $filter=[]): float
    {
        return (float) array_sum(
            ArrayHelper::map(
                $this->getItems()->andFilterWhere($filter)->all(),
                'lineWorth',
                'lineWorth'
            )
        );
    }

    public function getItemInstance(EndpointInterface $product, $qty, $sku=null, array $options): CartItem
    {
        return new CartItem([
            'product_id' => $product->getPrimaryKey(),
            'productModelClass' => get_class($product),
            'qty' => $qty,
            'unit_price' => $product->getPrice($options),
            'product_raw_data' => $product->getRaw(),
            'sku' => $sku,
            'options' => $options
        ]);
    }

    public function getItems(): ActiveQuery
    {
        return $this->hasMany(CartItem::class, ['pid' => 'id']);
    }

    public function getItem($id): ?CartItem
    {
        return CartItem::find()->where(['id' => $id,'pid' => $this->id])->one();
    }

    public function getItemByProduct(EndpointInterface $item, $sku=null): ?CartItem
    {
        return $this->hasOne(CartItem::class, ['pid' => 'id'])
            ->andWhere(['product_id' => $item->getPrimaryKey()])
            ->andWhere(['productModelClass' => get_class($item)])
            ->andFilterWhere(['sku' => $sku])
            ->one()
        ;
    }

    public function addItem(CartSettings $cartSettings, EndpointInterface $item, $qty=1, $sku=null, array $options=[]): self
    {
        if ($this->isNewRecord) {
            $this->save();
        }

        $item = $this->getItemInstance($item, $qty, $sku, $options);

        $this->link('items', $item);
        $item->link('cartSettings', $cartSettings);

        return $this;
    }

    public function removeItem(CartItem $item): self
    {
        $this->unlink('items', $item, true);

        return $this;
    }

    public function getAddLinkParms(CartSettings $cartSettings, EndpointInterface $item, int $qty, $sku=null, array $options=[]): array
    {
        return [
            'method' => 'post',
            'params' => [
                'product_id' => $item->getPrimaryKey(),
                'sku' => $sku,
                'qty' => $qty,
                'tsCartAction' => self::ACTION_ADD,
                'cartSettings' => $cartSettings->id,
                'product_options' => base64_encode(Json::encode($options))
            ]
        ];
    }
    public function getRemoveLinkParms(CartSettings $cartSettings, EndpointInterface $product, $sku=null): array
    {
        $cartItem = $this->getItemByProduct($product, $sku);

        return [
            'method' => 'post',
            'params' => [
                'cartItemId' => $cartItem->getPrimaryKey(),
                'tsCartAction' => self::ACTION_REMOVE,
                'cartSettings' => $cartSettings->id
            ]
        ];
    }
}
