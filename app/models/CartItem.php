<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\Json;
use ts\catalog\interfaces\EndpointInterface;

class CartItem extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return Cart::tableName();
    }

    public function getCart(): ActiveQuery
    {
        return $this->hasOne(Cart::class, ['id' => 'pid']);
    }

    public function getCartSettings(): ActiveQuery
    {
        return $this->hasOne(CartSettings::class, ['id' => 'settings_id']);
    }

    public function beforeSave($insert)
    {
        if (is_array($this->product_raw_data)) {
            $this->product_raw_data = Json::encode($this->product_raw_data);
        }

        if (is_array($this->options)) {
            $this->options = Json::encode($this->options);
        }

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->product_raw_data = Json::decode($this->product_raw_data);
        $this->options = Json::decode($this->options);

        return parent::afterFind();
    }

    public function getLineWorth(): float
    {
        return round($this->unit_price*$this->qty, 2);
    }

    public function getProduct(): EndpointInterface
    {
        $model = $this->productModelClass;
        $product = $model::getInstance($this->cartSettings->config->connection);
        $product->hydrate($this->product_raw_data);

        return $product;
    }

    public function rules()
    {
        return [
            [['pid','product_id','sku','productModelClass','qty','unit_price','product_raw_data'],'required'],
            ['unit_price','number','integerOnly' => false],
            ['qty','number','integerOny' => true],
            ['options','safe']
        ];
    }
}
