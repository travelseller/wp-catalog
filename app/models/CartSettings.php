<?php

declare(strict_types=1);

namespace ts\catalog\models;

class CartSettings extends ProductLister
{
    public function formFields(): array
    {
        $fields = parent::formFields();
        unset($fields['randomize']);

        $fields[] = [
            'attribute' => 'cartAddedText'
        ];

        return $fields;
    }


    public function getCart(string $sessId): Cart
    {
        $cart = Cart::find()->where(['sessId' => $sessId])->one();

        if (!$cart) {
            $cart = new Cart([
                'sessId' => $sessId,
            ]);
        }

        return $cart;
    }
}
