<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;
use ts\catalog\interfaces\EndpointInterface;

class Config extends BaseConfig
{
    public function rules()
    {
        return [
            [['pid','connection_id','endpointType'],'required'],
            ['connection_id','exist','targetRelation' => 'connection'],
            ['connection_id','checkValid'],
            ['endpointType','in','range' => array_keys($this->getEndpointOptions())],
        ];
    }

    /**
     * check if the selected Connection is valid.
     * In WP-Paradigma, it is possible to store invalid values.
     */
    public function checkValid()
    {
        if (!$this->connection->validate()) {
            $this->addError('connection_id', 'Connection is invalid! Check Conn-Settings!');
        }
    }

    public function getConnection()
    {
        return $this->hasOne(Connection::class, [
            'id' => 'connection_id'
        ]);
    }

    public function getEndpointOptions(): array
    {
        return [
            'product' => 'Product',
            'service' => 'Services',
            'hotel'   => 'Hotel'
        ];
    }

    public function getEndpoint(): EndpointInterface
    {
        switch ($this->endpointType) {
            case "service":
                return ServiceProduct::getInstance($this->connection);
                break;
            case "product":
                return StandardProduct::getInstance($this->connection);
                break;
            case "hotel":
                return HotelProduct::getInstance($this->connection);
                break;
            default:
                throw new \Exception("Unsupported Endpointtype: " .$this->endpointType);
        }
    }

    public function formFields(): array
    {
        return [
            [
                'attribute' => 'connection_id',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => ArrayHelper::map(
                    (new Connection())->findByState(['draft','publish','inherit'])->all(),
                    'id',
                    function ($model) {
                        return $model->getTitle() . ' (' . $model->wpPost->post_status . ')';
                    }
                )
            ],
            [
                'attribute' => 'endpointType',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => $this->getEndpointOptions(),
            ],
            [
                'group' => true,
                'label' => '<div class="separator">' . Yii::t('tsCatalog', 'Cache Settings') .  '</div>',
            ],
        ];
    }

    public function getLister()
    {
        return $this->hasOne(ProductLister::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductLister::class]);
    }

    public function getReader()
    {
        return $this->hasOne(ProductReader::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductReader::class]);
    }

    public function getFilter()
    {
        return $this->hasOne(ProductFilter::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductFilter::class]);
    }

    public function getCartSettings()
    {
        return $this->hasOne(CartSettings::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => CartSettings::class]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'connection_id' => Yii::t('tsCatalog', 'connection'),
        ]);
    }
}
