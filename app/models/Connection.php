<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use kartik\detail\DetailView;
use yii\httpclient\Client;
use ts\catalog\traits\WordPressTrait;
use ts\catalog\interfaces\WpPostInterface;
use ts\catalog\components\Request;

class Connection extends ActiveRecord implements WpPostInterface
{
    use WordPressTrait;

    public const CACHE_TYPE_FS = 'fs_cached';
    public const CACHE_TYPE_REDIS = 'redis_cached';

    public static function getWpType(): string
    {
        return 'tsconnection';
    }

    public static function tableName()
    {
        return '{{%tsibe_connections}}';
    }

    public function rules()
    {
        return [
            [['pid','endpoint_url','apiKey','cacheStrategy','cacheDuration'],'required'],
            ['cacheDuration','number','integerOnly' => true],
            [['brandId','partner_id'],'string'],
            ['endpoint_url','url'],
            ['cacheStrategy','in','range' => array_keys($this->getCacheOptions())],
            [['redisHost','redisPort','redisDb'],'required','when' => function ($model) {
                return $this->cacheStrategy === self::CACHE_TYPE_REDIS;
            }]

        ];
    }

    public function getTitle()
    {
        return $this->wpPost->post_title;
    }

    public function formFields(): array
    {
        return [
            ['attribute' => 'endpoint_url'],
            ['attribute' => 'apiKey'],
            ['attribute' => 'brandId'],
            ['attribute' => 'partner_id'],
            [
                'attribute' => 'cacheStrategy',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => $this->getCacheOptions(),
                'options' => ['onChange' => 'form.submit()']
            ],
            ['attribute' => 'cacheDuration'],
            [
                'attribute' => 'redisHost',
                'visible' => $this->cacheStrategy === self::CACHE_TYPE_REDIS
            ],
            [
                'attribute' => 'redisPort',
                'visible' => $this->cacheStrategy === self::CACHE_TYPE_REDIS
            ],
            [
                'attribute' => 'redisDb',
                'visible' => $this->cacheStrategy === self::CACHE_TYPE_REDIS
            ],
        ];
    }

    /**
     * find by WP-States.
     */
    public function findByState(array $states): \yii\db\ActiveQuery
    {
        $query = self::find();
        $query->leftJoin('{{%posts}}', '{{%posts}}.ID = ' . $this->tableName() . '.pid');
        $query->where(['{{%posts}}.post_status' => $states]);
        $query->andWhere(['{{%posts}}.post_type' => self::getWpType()]);

        return $query;
    }

    public function getCacheOptions()
    {
        return [
            self::CACHE_TYPE_FS => Yii::t('tsCatalog', 'File Cache'),
            self::CACHE_TYPE_REDIS => Yii::t('tsCatalog', 'Redis Cache'),
        ];
    }

    public function getCacheServiceConfig()
    {
        if ($this->cacheStrategy === self::CACHE_TYPE_REDIS) {
            return [
                'class' => 'yii\redis\Cache',
                'defaultDuration' => $this->cacheDuration,
                'redis' => [
                    'hostname' => $this->redisHost,
                    'port' => $this->redisPort,
                    'database' => $this->redisDb,
                ]
            ];
        }

        // Default is FileCache
        return [
            'class' => 'yii\caching\FileCache',
            'cacheFileSuffix' => 'wpTsCatalog',
            'defaultDuration' => $this->cacheDuration,
        ];
    }

    /**
     * Query the REST Endpoint.
     *
     * @param string endpoint (check /restapi/overview for possible endpoints)
     *
     * @param array $params The Query params, where Key => value will be send.
     * To send '<mode>get_all_regions</mode>' your params-array should be
     * ['mode' => 'get_all_regions']
     *
     * @param array $heaer Header params, where Key => value will be send.
     *
     * @return mixed array|string|null
     */
    public function query($endpoint, array $params=[], array $header=[], bool $enableCache=true)
    {
        $cacheKey = 'tsCatalog/' . crc32($this->endpoint_url.'/'.$endpoint . Json::encode($params));
        Yii::$app->set('cache', $this->getCacheServiceConfig());

        if (
            !$enableCache
            || (defined("WP_DEBUG") && WP_DEBUG)
            || !($result = Yii::$app->cache->get($cacheKey))
        ) {
            $request = $this->getRequest('get', $endpoint, $params, $header);

            $response = $request->send();
            if ($response->getStatusCode() != 200) {
                return [];
            }
            $result = $response->getData();

            if ($enableCache) {
                Yii::$app->cache->set($cacheKey, $result);
            }
        }

        return $result;
    }

    public function getRequest(string $method='get', string $endpoint, array $params, array $header): Request
    {
        $client = new Client([
            'requestConfig' => ['class' => 'ts\catalog\components\Request']
        ]);

        $header['apiKey'] = $this->apiKey;

        if ($this->brandId && !isset($header['brandId'])) {
            $header['brandId'] = $this->brandId;
        }

        if ($this->partner_id && !isset($header['partnerId'])) {
            $header['partnerId'] = $this->partner_id;
        }

        if ($method === 'post') {
            return $client->post(
                $this->endpoint_url . '/' . $endpoint,
                $params,
                $header
            );
        } else {
            return $client->get(
                $this->endpoint_url . '/' . $endpoint,
                $params,
                $header
            );
        }
    }

    public function attributeLabels()
    {
        return [
            'cacheStrategy' => Yii::t('tsCatalog', 'cache strategy'),
            'cacheDuration' => Yii::t('tsCatalog', 'cache duration in sec.'),
            'redisHost'     => Yii::t('tsCatalog', 'Redis Host'),
            'redisPort'     => Yii::t('tsCatalog', 'Redis Port'),
            'redisDb'       => Yii::t('tsCatalog', 'Redis Database')
        ];
    }
}
