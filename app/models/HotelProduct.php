<?php

declare(strict_types=1);

namespace ts\catalog\models;

use ts\catalog\interfaces\EndpointInterface;

class HotelProduct extends StandardProduct implements EndpointInterface
{
    public const LISTER_ENDPOINT = 'getHotelList';
    public const READER_ENDPOINT = 'getHotelInfo';

    public ?float $price_from = null;

    public function getPrice(array $options=[]): ?float
    {
        return $this->price_from;
    }
}
