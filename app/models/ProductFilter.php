<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii\helpers\FileHelper;
use ts\catalog\interfaces\EndpointInterface;
use kartik\detail\DetailView;

class ProductFilter extends BaseConfig
{
    /**
     * @var int region id
     */
    public $regionid;

    /**
     * @var int topic id (Thema Id)
     */
    public $themeid;

    /**
     * @var string productids (csv)
     */
    public $idlist;

    /**
     * @var string keyword search
     */
    public $search;

    /**
     * @var integer Hotelstars
     */
    public $hotelstars;

    /**
     * timespan from/to
     * @var string $timespan the Calendar-String, e.g.: "2021-01-01 - 2021-12-31"
     */
    public $timespan;

    /**
     * @var string Filterparam Date from
     * if value is kind of php:Y-m-d, it will be passed to date()
     */
    public $from;

    /**
     * @var string Filterparam Date to
     * see $from
     */
    public $to;

    /**
     * @var string duration in days, seperated by "-":
     * 1-2 (1 till 2 Days)
     * 7 (7 Days)
     * 10-14 (10 till 14 Days)
     */
    public $days;

    public const TYPE_REGION = 'regionid';
    public const TYPE_THEME = 'themeid';

    public function rules()
    {
        return [
            ['module_template','required'],
            [['hideWhenReaderActive'],'boolean'],
            ['filterKey','default','value' => $this->formName()],
            [['filterKey','defaultFilterParams'],'string'],
            ['jumpTo','safe'],
            [['regionid','themeid','idlist','search','from','to','hotelstars','days'],'safe'],
            ['nav_boxid_whitelist','each','rule' => ['integer']],
            ['nav_filter_type','in','range' => array_keys($this->getAvailFilter())],
            ['nav_lvl1_as_menuitem','boolean'],
        ];
    }

    public function beforeValidate()
    {
        $this->ensureBoxIdIsArray();
        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        $this->ensureBoxIdIsString();
        return parent::afterValidate();
    }

    protected function ensureBoxIdIsArray()
    {
        if ($this->nav_boxid_whitelist && !is_array($this->nav_boxid_whitelist)) {
            $this->nav_boxid_whitelist = explode(",", $this->nav_boxid_whitelist);
        }
    }

    protected function ensureBoxIdIsString()
    {
        if ($this->nav_boxid_whitelist && is_array($this->nav_boxid_whitelist)) {
            $this->nav_boxid_whitelist = implode(",", $this->nav_boxid_whitelist);
        }
    }

    public function getAvailFilter()
    {
        return [
            self::TYPE_REGION => Yii::t('tsCatalog', self::TYPE_REGION),
            self::TYPE_THEME => Yii::t('tsCatalog', self::TYPE_THEME),
        ];
    }

    public function formFields(): array
    {
        return [
            [
                'attribute' => 'jumpTo',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'options' => [
                    'prompt' => Yii::t('tsCatalog', 'same as block'),
                ],
                'items' => ArrayHelper::map(
                    (new WpPost())->findPostPage()->all(),
                    'ID',
                    function ($model) {
                        return $model->ID . ' ' . $model->post_title . ' ' . ' (' . $model->post_name . ')';
                    }
                )
            ],
            [
                'group' => true,
                'label' => '<div class="separator">' . Yii::t('tsCatalog', 'Layout Settings') .  '</div>',
            ],
            [
                'attribute' => 'module_template',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => ArrayHelper::map(
                    Template::find()->all(),
                    'id',
                    'title'
                )
            ],
            [
                'attribute' => 'filterKey',
            ],
            [
                'attribute' => 'defaultFilterParams',
            ],
            [
                'attribute' => 'hideWhenReaderActive',
                'type' => DetailView::INPUT_CHECKBOX,
                'format' => 'bool',
                'options' => ['label' => false]
            ],
            [
                'group' => true,
                'label' => '<div class="separator">' . Yii::t('tsCatalog', 'Navigation Settings') .  '</div>',
            ],
            [
                'attribute' => 'nav_filter_type',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => $this->getAvailFilter()
            ],
            [
                'attribute' => 'nav_boxid_whitelist'
            ],
            [
                'attribute' => 'nav_lvl1_as_menuitem',
                'type' => DetailView::INPUT_CHECKBOX,
                'format' => 'bool',
                'options' => ['label' => false]
            ],

        ];
    }

    public function getJumpToPageUrl($filterParams=[], $keepGetParams=true): string
    {
        global $wp;

        $getParams = [];
        if ($keepGetParams) {
            $getParams = Yii::$app->request->get() ?? [];
        }

        $filterParams = array_merge($getParams, $filterParams);

        $jumpTo = home_url($wp->request);

        if ($this->jumpTo) {
            $jumpTo = get_permalink($this->jumpTo);
        }

        if (!$filterParams) {
            return $jumpTo;
        }

        return $jumpTo . (strpos($jumpTo, '?') ? '&' : '?') . http_build_query($filterParams);
    }

    public function getLister()
    {
        return $this->hasOne(ProductLister::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductLister::class]);
    }

    public function getReader()
    {
        return $this->hasOne(ProductReader::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductReader::class]);
    }

    public function getTemplate($type='module_template')
    {
        return $this->hasOne(Template::class, [
            'id' => $type
        ]);
    }

    /**
     * Query the endpoint "getOffers".
     * Dont cast arrays to csv! Some api attributes
     * expects arrays, e.g.: "timespan" and "pricespan"
     *
     * @param array $params key/value to submit
     * @param EndpointInterface $ep bypass the configurated Endpoint with this one
     */
    public function search(array $params=[], ?EndpointInterface $ep = null): ArrayDataProvider
    {
        $this->attributes = $params;
        $defaultFilter = [];
        foreach (explode(
            ";",
            $this->defaultFilterParams ?? ''
        ) as $defaultParams) {
            $pairs = explode("=", $defaultParams);

            if (!isset($pairs[0]) || !isset($pairs[1])) {
                continue;
            }

            $defaultFilter[$pairs[0]] = $pairs[1];
        }

        $params = array_merge($defaultFilter, $params);

        if (strncmp(ArrayHelper::getValue($params,'from',''),'php:',4) === 0) {
            $format = substr(ArrayHelper::getValue($params,'from'), 4);
            $params['from'] = date($format);
        }

        if (strncmp(ArrayHelper::getValue($params,'to',''),'php:',4) === 0) {
            $format = substr(ArrayHelper::getValue($params,'to'), 4);
            $params['to'] = date($format);
        }

        $endpoint = $ep ?? $this->config->endpoint;
        $result = $endpoint->findAll($params);

        return new ArrayDataProvider([
            'allModels' => $result ?? []
        ]);
    }

    public function getNavItems($parms=[]): array
    {
        $items = $this->nav_filter_type === self::TYPE_REGION ? $this->getRegions($parms) : $this->getThemes($parms);
        return $items;
    }

    public function getItemInfo(int $id)
    {
        if ($this->nav_filter_type === self::TYPE_REGION) {
            return $this->config->connection->query('getRegionInfo/'.$id);
        } else {
            return $this->config->connection->query('getThemeInfo/'.$id);
        }
    }

    public function getRegions($parms=[])
    {
        $result = $this->config->connection->query('getRegions', $parms);

        $this->ensureBoxIdIsArray();
        if ($this->nav_boxid_whitelist && count($this->nav_boxid_whitelist)) {
            $result = $this->stripNavItems($result, $this->nav_boxid_whitelist);
        }
        return $result;
    }

    public function getThemes($parms=[])
    {
        $result = $this->config->connection->query('getThemes', $parms);
        $this->ensureBoxIdIsArray();
        if ($this->nav_boxid_whitelist && count($this->nav_boxid_whitelist)) {
            $result = $this->stripNavItems($result, $this->nav_boxid_whitelist);
        }
        return $result;
    }

    protected function stripNavItems(array $items, array $whitelist)
    {
        foreach ($items as $key => $item) {
            if (!in_array($item['box'], $this->nav_boxid_whitelist)) {
                unset($items[$key]);
            }
        }

        return $items;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'jumpTo' => Yii::t('tsCatalog', 'jump to'),
            'filterKey' => Yii::t('tsCatalog', 'Queryparamter'),
            'defaultFilterParams' => Yii::t('tsCatalog', 'default filter'),
            'hideWhenReaderActive' => Yii::t('tsCatalog', 'hide when Reader active?'),
        ]);
    }
}
