<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use kartik\detail\DetailView;

class ProductLister extends BaseConfig
{
    public function rules()
    {
        return [
            [['pid','module_template','item_template'],'required'],
            [['enablePjax','usePager','hideWhenReaderActive','randomize'],'boolean'],
            ['pagerSize','number','integerOnly' => true],
            ['pagerSize','default','value' => 20],
            ['usePager','default','value' => true],
            ['pagerParam','default','value' => 'tsIbePage'],
            [
                [
                    'jumpTo',
                    'listerContainerDataAttributes',
                    'moduleCssClasses',
                    'moduleCssItemClasses',
                    'pagerContainerCssClasses',
                    'pagerLinkContainerCssClasses',
                    'pagerLinkCssClasses',
                    'pagerLinkDataAttributes',
                    'pagerParam'
                ],
                'safe'
            ]
        ];
    }

    public function getTemplate()
    {
        return $this->hasOne(Template::class, [
            'id' => 'module_template'
        ]);
    }

    public function getItemTemplate()
    {
        return $this->hasOne(Template::class, [
            'id' => 'item_template'
        ]);
    }

    public function getDetaillink($itemId): string
    {
        global $wp;

        $detailLink = home_url($wp->request);

        if ($this->jumpTo) {
            $detailLink = get_permalink($this->jumpTo);
        }

        $readerParams = explode("|", $this->reader->readerParam);
        return $detailLink . (strpos($detailLink, '?') ? '&' : '?') . http_build_query([$readerParams[0] => $itemId]);
    }

    public function formFields(): array
    {
        return [
            [
                'attribute' => 'jumpTo',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'options' => [
                    'prompt' => Yii::t('tsCatalog', 'same as block'),
                ],
                'items' => ArrayHelper::map(
                    (new WpPost())->findPostPage()->all(),
                    'ID',
                    function ($model) {
                        return $model->ID . ' ' . $model->post_title . ' ' . ' (' . $model->post_name . ')';
                    }
                )
            ],
            [
                'group' => true,
                'label' => '<div class="separator">' . Yii::t('tsCatalog', 'Layout Settings') .  '</div>',
            ],
            [
                'attribute' => 'module_template',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => ArrayHelper::map(
                    Template::find()->all(),
                    'id',
                    'title'
                )
            ],
            [
                'attribute' => 'moduleCssClasses'
            ],
            [
                'attribute' => 'moduleCssItemClasses'
            ],
            [
                'attribute' => 'listerContainerDataAttributes'
            ],
            [
                'attribute' => 'item_template',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => ArrayHelper::map(
                    Template::find()->all(),
                    'id',
                    'title'
                )
            ],
            [
                'attribute' => 'hideWhenReaderActive',
                'type' => DetailView::INPUT_CHECKBOX,
                'format' => 'bool',
                'options' => ['label' => false]
            ],
            'randomize' => [
                'attribute' => 'randomize',
                'type' => DetailView::INPUT_CHECKBOX,
                'format' => 'bool',
                'options' => ['label' => false]
            ],
            [
                'group' => true,
                'label' => '<div class="separator">' . Yii::t('tsCatalog', 'Pager Settings') .  '</div>',
            ],
            [
                'attribute' => 'usePager',
                'type' => DetailView::INPUT_CHECKBOX,
                'format' => 'bool',
                'options' => [
                    'label' => false,
                    'onChange' => 'form.submit()'
                ],
            ],
            [
                'attribute' => 'pagerContainerCssClasses',
                'visible' => $this->usePager
            ],
            [
                'attribute' => 'pagerLinkContainerCssClasses',
                'visible' => $this->usePager
            ],
            [

                'attribute' => 'pagerLinkCssClasses',
                'visible' => $this->usePager
            ],
            [
                'attribute' => 'pagerLinkDataAttributes',
                'visible' => $this->usePager
            ],
            [
                'attribute' => 'pagerSize',
                'visible' => $this->usePager
            ],
            [
                'attribute' => 'pagerParam',
                'visible' => $this->usePager
            ],
            /*
            [
                'attribute' => 'enablePjax',
                'type' => DetailView::INPUT_CHECKBOX,
                'format' => 'bool',
                'options' => ['label' => false]
            ],
            */

        ];
    }

    public function getReader()
    {
        return $this->hasOne(ProductReader::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductReader::class]);
    }

    public function getFilter(): \yii\db\ActiveQuery
    {
        return $this->hasOne(ProductFilter::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductFilter::class]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'jumpTo' => Yii::t('tsCatalog', 'jump to'),
            'moduleCssClasses' => Yii::t('tsCatalog', 'container css classes'),
            'moduleCssItemClasses' => Yii::t('tsCatalog', 'container item css classes'),
            'hideWhenReaderActive' => Yii::t('tsCatalog', 'hide when Reader active?'),
            'listerContainerDataAttributes' => Yii::t('tsCatalog', 'Lister Data-Attributes'),
            'pagerContainerCssClasses' => Yii::t('tsCatalog', 'Container CSS Classes'),
            'pagerLinkContainerCssClasses'      => Yii::t('tsCatalog', 'Link-Container CSS Classes'),
            'pagerLinkCssClasses'       => Yii::t('tsCatalog', 'Link CSS Classes'),
            'pagerLinkDataAttributes'       => Yii::t('tsCatalog', 'Pagerlink Data-Attributes'),
            'pagerSize'     => Yii::t('tsCatalog', 'Pagersize'),
            'pagerParam'        => Yii::t('tsCatalog', 'Queryparameter'),
            'usePager' => Yii::t('tsCatalog', 'use pager?'),
            'randomize' => Yii::t('tsCatalog', 'randomize result?'),
        ]);
    }
}
