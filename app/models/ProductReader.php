<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii\helpers\FileHelper;
use kartik\detail\DetailView;

class ProductReader extends BaseConfig
{
    public function rules()
    {
        return [
            [['pid','module_template'],'required'],
            ['readerParam','default','value' => 'tsIbeOffer'],
            ['error_template','safe']
        ];
    }

    public function getLister()
    {
        return $this->hasOne(ProductLister::class, [
            'pid' => 'pid'
        ])->andWhere(['modelClass' => ProductLister::class]);
    }

    public function getTemplate()
    {
        return $this->hasOne(Template::class, [
            'id' => 'module_template'
        ]);
    }

    public function getErrorTemplate()
    {
        return $this->hasOne(Template::class, [
            'id' => 'error_template'
        ]);
    }

    public function formFields(): array
    {
        return [
            [
                'attribute' => 'module_template',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => ArrayHelper::map(
                    Template::find()->all(),
                    'id',
                    'title'
                )
            ],
            [
                'attribute' => 'error_template',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => ArrayHelper::map(
                    Template::find()->all(),
                    'id',
                    'title'
                )
            ],
            [
                'attribute' => 'readerParam',
            ],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'readerParam' => Yii::t('tsCatalog', 'Queryparamter'),
            'error_template' => Yii::t('tsCatalog', 'error template'),
        ]);
    }
}
