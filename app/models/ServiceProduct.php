<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\helpers\ArrayHelper;
use ts\catalog\interfaces\EndpointInterface;
use DateTime;

class ServiceProduct extends StandardProduct implements EndpointInterface
{
    public const LISTER_ENDPOINT = 'getServices';
    public const READER_ENDPOINT = 'getServices';

    public ?float $price_from = null;

    public $date;


    /**
     * {@inheritdoc}
     */
    public function findOne($id): ?EndpointInterface
    {
        if (is_array($id)) {
            $date = ArrayHelper::getValue($id, 'date');
            $date = DateTime::createFromFormat('Y-m-d\TH:i:s.u+', $date);
            $variantTitle = ArrayHelper::getValue($id, 'variant');
            $id = $id['id'];
        }

        $result = $this->connection->query(static::READER_ENDPOINT . '/' . $id, [], ['Textout' => 'html']);

        if (!$result) {
            return null;
        }

        if (!ArrayHelper::isAssociative($result)) {
            $result = $result[0];
        }

        $obj = new static();

        $dates = ArrayHelper::getValue($result, 'dates', []);
        if (!is_array($dates) || !$dates) {
            $obj->hydrate($result);
            return $obj;
        }

        foreach (ArrayHelper::getValue($result, 'dates', []) as $dateConf) {
            if (isset($date) && $date && $dateConf['date'] !== $date->format('Y-m-d')) {
                continue;
            }

            foreach ($dateConf['variants'] as $variant) {
                if (isset($variantTitle) && $variant['title'] !== $variantTitle) {
                    continue;
                }

                $obj = new static();
                $itemConf = $result;
                $itemConf = ArrayHelper::merge($itemConf, $dateConf);
                $itemConf['price'] = $variant['price'];
                $obj->hydrate($itemConf);
                return $obj;
            }
        }

        $obj->hydrate($result);
        return $obj;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $filter=[]): array
    {
        $result = $this->connection->query(static::LISTER_ENDPOINT, $filter, ['Textout' => 'html']);
        $buffer=[];
        foreach ($result as $item) {
            $dates = ArrayHelper::getValue($item, 'dates', []);
            if (!is_array($dates) || !$dates) {
                $obj = new static();
                $obj->hydrate($item);
                $buffer[] = $obj;
                continue;
            }
            foreach ($dates as $dateConf) {
                $obj = new static();
                $itemConf = $item;
                $itemConf = array_merge($itemConf, $dateConf);
                $obj->hydrate($itemConf);
                $buffer[] = $obj;
            }
        }
        return $buffer;
    }

    public function getPrice(array $options=[]): ?float
    {
        if (!isset($options['variant'])) {
            return (float) $this->price_from;
        }

        foreach (ArrayHelper::getValue($this->raw, 'variants', []) as $option) {
            if ($option['title'] !== $options['variant']) {
                continue;
            }

            return (float) $option['price'];
        }

        return (float) $this->price_from;
    }
}
