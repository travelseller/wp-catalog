<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\helpers\ArrayHelper;
use ts\catalog\interfaces\EndpointInterface;

class StandardProduct extends \yii\base\Model implements EndpointInterface
{
    public $connection;

    public ?int $id = null;

    public ?float $price = null;

    public array $raw = [];

    public const LISTER_ENDPOINT = 'getOffers';
    public const READER_ENDPOINT = 'getProductFull';

    public static function getInstance(Connection $connection): EndpointInterface
    {
        return new static([
            'connection' => $connection
        ]);
    }

    public function getPrimaryKey()
    {
        return $this->id;
    }

    public function getRaw(): array
    {
        return $this->raw;
    }

    public function getPrice(array $options=[]): ?float
    {
        return $this->price;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $filter=[]): array
    {
        $result = $this->connection->query(static::LISTER_ENDPOINT, $filter, ['Textout' => 'html']);
        $buffer=[];
        foreach ($result as $item) {
            $obj = new static();
            $obj->hydrate($item);
            $buffer[] = $obj;
        }
        return $buffer;
    }

    /**
     * {@inheritdoc}
     */
    public function findOne($id): ?EndpointInterface
    {
        if (is_array($id)) {
            $id = $id['id'];
        }

        $result = $this->connection->query(static::READER_ENDPOINT . '/' . $id, [], ['Textout' => 'html']);
        if (!$result) {
            return null;
        }
        if (!ArrayHelper::isAssociative($result)) {
            $result = $result[0];
        }
        $obj = new static();
        $obj->hydrate($result);
        return $obj;
    }

    public function hydrate(array $rawData)
    {
        foreach ($rawData as $attr => $value) {
            if ($this->hasProperty($attr)) {
                $this->{$attr} = $value;
            }
        }
        $this->raw = $rawData;
    }
}
