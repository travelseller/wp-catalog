<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use kartik\detail\DetailView;
use ts\catalog\traits\WordPressTrait;
use ts\catalog\interfaces\WpPostInterface;

class Template extends ActiveRecord implements WpPostInterface
{
    use WordPressTrait;

    public $templateContent;

    /**
     * Copy content from a existing template
     * This array contains all available templates
     */
    public $availForCopy = [];

    public function init()
    {
        add_action('wp_trash_post', function ($post_id) {
            $obj = self::find()->where(['pid' => $post_id])->one();
            if ($obj) {
                $obj->delete();
            }
        });
    }

    /**
     * Dont compile @-prefixed path here.
     * Otherwise, a controller is needed to locate the viewfile.
     * @see yii\base\View:172
     */
    public function getFilePath()
    {
        return '@ts/catalog/views/customs/' . $this->pid . '.php';
    }

    public function afterDelete()
    {
        if (file_exists(Yii::getAlias($this->getFilePath()))) {
            unlink(Yii::getAlias($this->getFilePath()));
        }

        return parent::afterDelete();
    }

    public static function getWpType(): string
    {
        return 'tstemplate';
    }

    public static function tableName()
    {
        return '{{%tsibe_templates}}';
    }

    public function rules()
    {
        return [
            [['title','pid','templateContent'],'required'],
            ['availForCopy','in','range' => array_keys($this->getTemplates())]
        ];
    }

    public function beforeSave($insert)
    {
        $this->title = $this->wpPost->post_title;

        if (in_array($this->wpPost->post_status, ['auto-draft','trash'])) {
            return false;
        }

        if ($this->availForCopy) {
            file_put_contents(
                Yii::getAlias($this->getFilePath()),
                file_get_contents($this->availForCopy)
            );
        } elseif ($this->templateContent) {
            $content = stripslashes(
                html_entity_decode($this->templateContent, ENT_QUOTES, 'UTF-8')
            );

            file_put_contents(
                Yii::getAlias($this->getFilePath()),
                $content
            );
        }

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        if (file_exists(Yii::getAlias('@ts/catalog/views/customs/' . $this->pid . '.php'))) {
            $this->templateContent = file_get_contents(Yii::getAlias($this->getFilePath()));
        }

        return parent::afterFind();
    }

    public function formFields(): array
    {
        return [
            [
                'attribute' => 'availForCopy',
                'type' => DetailView::INPUT_DROPDOWN_LIST,
                'items' => $this->getTemplates(),
                'options' => [
                    'prompt' => Yii::t('tsCatalog', 'please select ...'),
                    'onChange' => 'form.submit()',
                ]
            ],
            [
                'attribute' => 'templateContent',
                'type' => DetailView::INPUT_TEXTAREA,
                'options' => ['style' => 'width: 100%;height:400px']
            ],
        ];
    }

    public function getTemplates()
    {
        // templates shipped by package
        $shipped = FileHelper::findFiles(Yii::getAlias('@ts/catalog/views/defaults'), [
            'only' => ['*.php']
        ]);

        $custom = self::find()->all();

        $out = [];
        foreach ($shipped as $path) {
            $out[$path] = basename($path, '.php') . ' (core)';
        }

        foreach ($custom as $objTemplate) {
            $file = Yii::getAlias('@ts/catalog/views/customs/') . $objTemplate->pid . '.php';
            $out[$file] = $objTemplate->wpPost->post_title ?? 'template for post ' . $objTemplate->pid;
        }

        return $out;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'availForCopy' => Yii::t('tsCatalog', 'copy content from template'),
            'templateContent' => Yii::t('tsCatalog', 'content'),

        ]);
    }
}
