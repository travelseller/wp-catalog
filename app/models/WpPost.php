<?php

declare(strict_types=1);

namespace ts\catalog\models;

use Yii;
use yii\db\ActiveRecord;

class WpPost extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%posts}}';
    }

    public function findPostPage(): yii\db\ActiveQuery
    {
        $finder = static::find();
        $finder->andWhere([
            'post_type' => ['post','page'],
            'post_status' => ['draft','publish','inherit']
        ]);

        return $finder;
    }
}
