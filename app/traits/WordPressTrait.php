<?php

declare(strict_types=1);

namespace ts\catalog\traits;

use Yii;
use ts\catalog\models\WpPost;

trait WordPressTrait
{
    public function getWpPost()
    {
        return $this->hasOne(WpPost::class, [
            'ID' => 'pid'
        ]);
    }
}
