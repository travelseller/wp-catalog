<?php
/**
 * @var int $index current index
 * @var int $key current array itemkey
 * @var string $detailLink link to product-details
 * @var closure $removeFromCartLink($linkTitle,$htmlOptions) generate a remove-item link
 * @var array $raw cart-item raw-stack
 * @var ts\catalog\models\CartItem $cartItem
 * @var ts\catalog\interfaces\EndpointInterface $product Object-Representation of REST-Entity
 * @var ts\catalog\models\CartSettings $cartSettings the Cart-Settings
 */
?>

<tr>
	<td><?=$product->getPrimaryKey();?>
	<td><a href="<?=$detailLink;?>"><?=$title;?></a></td>
	<td><?=$product->getPrice();?></td>
	<td><?=$removeFromCartLink('remove from cart', ['class' => 'btn btn-danger']);?>
</tr>