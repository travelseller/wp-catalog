<?php
/**
 * @var int $index current index
 * @var int $key current array itemkey
 * @var string $detailLink link to product-details
 * @var closure $removeFromCartLink($linkTitle,$htmlOptions) generate a cart remove-item link
 * @var closure $addToCartLink(string $linkTitle,int $quanty,array $htmlOptions,array $productOptions=[]) generate a cart add-item link
 * @var array $raw raw-stack
 * @var ts\catalog\interfaces\EndpointInterface $product Object-Representation of REST-Entity
 * @var ts\catalog\models\Cart $cart the Cart
 * @var ts\catalog\models\ProductLister $lister
 */
?>

<div class="item <?=$key;?>" style="margin-bottom: 30px">
	<h3><?=($titel ?? '');?></h3>
	<?php if (isset($pic1_URL)):?>
		<a href="<?=$detailLink;?>">
			<div style="width:50%;float:left"><img src="<?=$pic1_URL;?>"></div>
		</a>
	<?php endif;?>
	<div style="width:50%;float:left"><?=($text_1 ?? '');?></div>
	<div style="clear: both"></div>
	<small><?=($id ?? '');?></small>
	<a href="<?=$detailLink;?>">Details</a>
</div>