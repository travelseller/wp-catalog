<?php
/**
 * @var $items string products
 * @var $item_container_id string the css-id used in container
 * @var $pager string pager html code
 * @var $summary string summary html code
 * @var $cart ts\catalog\models\Cart instance
 */
?>

<?php if (count($cart->items) > 0):?>
<table class="table">
	<thead>
		<tr>
			<th>id</th>
			<th>title</th>
			<th>price</th>
			<th></th>
		</tr>
	</thead>
	<?=$items;?>
</table>
<?php else: ?>
	<h3>your cart is empty!</h3>
<?php endif;?>