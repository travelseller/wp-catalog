<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var $jumpTo string filter url
 * @var $regions array regions
 * @var $themes array themes
 * @var $formname string the formular name
 * @var $filter \app\models\Filter
 * @var $themeDropdownFilter callable $themeDropdownFilter(array $items,array $options)
 * @var $regionDropdownFilter callable $themeDropdownFilter(array $items,array $options)
 *
 * callable $options:
 * Key is HTML-Attribute, value is value.
 *
 * echo $themeDropdownFilter(
 *	[['id' => 123,'title' => 'Wandern']],
 *	['class' => 'foo','id' => 'bar','data-baz' => 'bartaz']
 * );
 *
 * produces:
 *
 * <select class="foo" id="bar" data-baz="bartaz" name="ProductFilter[tid]">
 *   <option value="123">Wandern</option>
 * </select>
 */
?>

<form name="<?=$formname;?>" method="post" action="<?=$jumpTo;?>">
	<?=$themeDropdownFilter($themes, ['prompt' => 'Dont Filter']);?>
	<?=$regionDropdownFilter($regions, ['prompt' => 'Dont Filter']);?>
	<input type="submit" value="filter">
</form>