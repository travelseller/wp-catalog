<?php
/**
 * @var $items string products
 * @var $item_container_id string the css-id used in container
 * @var $pager string pager html code
 * @var $summary string summary html code
 * @var $filter ts\catalog\models\ProductFilter instance for this lister results
 */
?>

<div class="container">
	<?=$pager;?>
</div>

<div class="container">
	<?=$summary;?>
</div>

<div class="container">
	<?=$items;?>
</div>