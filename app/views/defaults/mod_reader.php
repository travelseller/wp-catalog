<?php
/**
 * @var closure $addToCartLink(string $linkTitle,int $quanty,array $htmlOptions,array $productOptions=[]) generate a cart add-item link
 * @var closure $removeFromCartLink($linkTitle,$htmlOptions) generate a cart remove-item link
 * @var array $raw raw-stack
 * @var ts\catalog\interfaces\EndpointInterface $product Object-Representation of REST-Entity
 * @var ts\catalog\models\Cart $cart the Cart
 * @var ts\catalog\models\ProductReader $reader
 */
?>

<h3><?=($titel ?? '');?></h3>
<div>
	<small><?=($text_1 ?? '');?></small>
	<?php if (isset($pic1_URL)):?>
		<div style="width:50%"><img src="<?=$pic1_URL;?>"></div>
	<?php endif;?>

	<?php if (isset($book_link_new_ibe)): ?>
		<a href="<?=$book_link_new_ibe;?>">Buchen</a>	
	<?php endif;?>

	<?php if (isset($reise_preis_info)):?>
		<?=$reise_preis_info;?>
	<?php endif;?>

</div>