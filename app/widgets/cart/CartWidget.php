<?php

namespace ts\catalog\widgets\cart;

use Yii;
use yii\bootstrap4\Html;

class CartWidget extends \WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            // Base ID of your widget
            'tscatalog_cart_widget',

            // Widget name will appear in UI
            'Cart Widget',

            // Widget description
            ['description' => 'Cartwidget displays a Cart']
        );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        if (!empty($instance['cssContainerClass'])) {
            if (strpos($args['before_widget'], "col")) {
                $args['before_widget'] = str_replace("col", $instance['cssContainerClass'], $args['before_widget']);
            } else {
                $args['before_widget'] = str_replace("class=\"", "class=\"". $instance['cssContainerClass'] ." ", $args['before_widget']);
            }
        }

        echo $args['before_widget'];

        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        echo Html::beginTag('div', ['class' => 'cart-wrapper bg-light-opera text-danger']);
        echo Html::tag('i', '', ['class' => 'fas fa-suitcase']);
        echo Html::tag('span', '0', ['class' => 'cartbadge badge badge-dark']);
        echo Html::endTag('div');

        echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }

        echo '<label for=" ' . $this->get_field_id('title') . '"> ' . _e('Title:') . ' </label> 
		<input class="widefat" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . esc_attr($title) . '" />';

        if (isset($instance[ 'cssContainerClass' ])) {
            $cssContainerClass = $instance['cssContainerClass'];
        } else {
            $cssContainerClass = '';
        }

        echo '<label for=" ' . $this->get_field_id('cssContainerClass') . '"> ' . _e('cssContainerClass:') . ' </label> 
		<input class="widefat" id="' . $this->get_field_id('cssContainerClass') . '" name="' . $this->get_field_name('cssContainerClass') . '" type="text" value="' . esc_attr($cssContainerClass) . '" />';
    }

    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['cssContainerClass'] = (!empty($new_instance['cssContainerClass'])) ? strip_tags($new_instance['cssContainerClass']) : '';
        return $instance;
    }
}
