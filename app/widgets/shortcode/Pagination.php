<?php

declare(strict_types=1);

namespace ts\catalog\widgets\shortcode;

use Yii;
use yii\web\Request;

class Pagination extends \yii\data\Pagination
{
    public $pageParam;

    public function createUrl($page, $pageSize = null, $absolute = false)
    {
        global $wp;

        $page = (int) $page;
        $pageSize = (int) $pageSize;
        if (($params = $this->params) === null) {
            $params = array_merge(Yii::$app->request->get(), Yii::$app->request->post());
        }
        if ($page > 0 || $page == 0 && $this->forcePageParam) {
            $params[$this->pageParam] = $page + 1;
        } else {
            unset($params[$this->pageParam]);
        }
        if ($pageSize <= 0) {
            $pageSize = $this->getPageSize();
        }
        if ($pageSize != $this->defaultPageSize) {
            $params[$this->pageSizeParam] = $pageSize;
        } else {
            unset($params[$this->pageSizeParam]);
        }

        return home_url($wp->request) . '?' . http_build_query($params);
    }
}
