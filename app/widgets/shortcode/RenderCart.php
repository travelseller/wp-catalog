<?php

declare(strict_types=1);

namespace ts\catalog\widgets\shortcode;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use kartik\detail\DetailView;
use ts\catalog\models\ProductReader;
use ts\catalog\models\CartSettings;
use ts\catalog\models\WpPost;

class RenderCart extends RenderLister
{
    /**
     * @var int WP PostId
     */
    public $postId;

    /**
     * @var array HTML-options
     * @see \yii\helpers\Html
     */
    public $options = [];

    /**
     * @var \yii\data\ArrayDataProvider
     */
    private $dp;

    /**
     * @var CartSettings
     */
    private $cartSettings;

    /**
     * @var Cart
     */

    private $cart;

    /**
     * @var Reader
     */
    private $reader;

    public function init()
    {
        $this->cartSettings = CartSettings::find()->andWhere(['pid' => $this->postId])->one();

        if (!$this->cartSettings) {
            throw new \yii\base\InvalidConfigException('No CartSettings for postId (pid) ' . $this->postId . ' found!');
        }

        $this->cart = $this->cartSettings->getCart(Yii::$app->session->id);

        $this->dp = new ArrayDataProvider([
            'allModels' => $this->cart->items ?? []
        ]);
    }

    public function run()
    {
        global $wp;

        if (
            $this->cartSettings->hideWhenReaderActive
            && Yii::$app->getProductIdSlug($this->cartSettings->reader)
        ) {
            echo '';
            return;
        }

        $items_id = (new ListView([
            'dataProvider' => $this->dp,
        ]))->getId();

        $items = ListView::widget([
            'id' => $items_id,
            'dataProvider' => $this->dp,
            'layout' => '{items}',
            'options' => [
                'class' => ArrayHelper::getValue($this->options, 'class') ?? $this->cartSettings->moduleCssClasses ?? 'list-view',
                'data' => $this->extractDataAttributes($this->cartSettings->listerContainerDataAttributes)
            ],
            'itemOptions' => ['class' => $this->cartSettings->moduleCssItemClasses ?? 'col'],
            'itemView' => function ($model, $key, $index, $widget) use ($wp) {
                $product = $model->product;
                $detailLink = $this->cartSettings->config->lister->getDetailLink($product->getPrimaryKey());
                $cart = $this->cartSettings->getCart(Yii::$app->session->id);
                return $widget->render(
                    $this->cartSettings->itemTemplate->filePath,
                    array_merge([
                        'index' => $index,
                        'key' => $key,
                        'cartItem' => $model,
                        'detailLink' => $detailLink,
                        'removeFromCartLink' => function ($linkTitle, $htmlOptions=[]) use ($cart, $product) {
                            ArrayHelper::setValue($htmlOptions, 'data', $cart->getRemoveLinkParms($this->cartSettings, $product));
                            return Html::a($linkTitle, '', $htmlOptions);
                        },
                        'raw' => $model->product->getRaw(),
                        'product' => $model->product,
                        'cartSettings' => $this->cartSettings
                    ], $model->product->getRaw())
                );
            }
        ]);

        /**
         * For Summary and Pager
         */
        $listView = new ListView([
            'dataProvider' => $this->dp,
            'pager' => [
                // Container Options
                'options' => ['class' => $this->cartSettings->pagerContainerCssClasses],
                // Item Options
                'linkContainerOptions' => ['class' => $this->cartSettings->pagerLinkContainerCssClasses],
                // ahref options
                'linkOptions' => [
                    'class' => $this->cartSettings->pagerLinkCssClasses,
                ]
            ]
        ]);

        $pager = $this->cartSettings->usePager ? $listView->renderSection('{pager}') : null;
        $summary = $listView->renderSection('{summary}');

        $view = $this->getView();
        $view->registerAssetBundle(\ts\catalog\CatalogAssetBundle::class);

        echo $view->render(
            $this->cartSettings->template->filePath,
            [
                'items' => $items,
                'items_container_id' => $items_id,
                'pager' => $pager,
                'summary' => $summary,
                'cart' => $this->cart
            ]
        );
    }
}
