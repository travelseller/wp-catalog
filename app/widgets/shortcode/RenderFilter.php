<?php

declare(strict_types=1);

namespace ts\catalog\widgets\shortcode;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use kartik\detail\DetailView;
use ts\catalog\models\ProductFilter;

/**
 * Displays Tabs in Backend for Setup an IBE with jquery ui.
 * Each Tab contains a Pjax-Container with a ActiveForm,
 * the FormFields are rendered via Model::formFields() Method.
 */
class RenderFilter extends Widget
{
    /**
     * @var int WP PostId
     */
    public $postId;

    /**
     * @var ProductFilter
     */
    private $_model;

    public function init()
    {
        $this->_model = ProductFilter::find()->andWhere(['pid' => $this->postId])->one();

        if (!$this->_model) {
            throw new \yii\base\InvalidConfigException('No ProductFilter for postId (pid) ' . $this->postId . ' found!');
        }

        $this->_model->setScenario('search');
        $this->_model->load(array_merge(
            Yii::$app->request->get(),
            Yii::$app->request->post()
        ));
    }

    public function run()
    {
        if (
            $this->_model->hideWhenReaderActive
            && Yii::$app->getProductIdSlug($this->_model->reader)
        ) {
            return;
        }

        $view = $this->getView();
        //$view->registerAssetBundle(\ts\catalog\CatalogAssetBundle::class);

        echo $view->render(
            $this->_model->template->filePath,
            [
                'formname' => $this->_model->formName(),
                'jumpTo' => $this->_model->getJumpToPageUrl(),
                'regions' => $this->_model->getRegions(),
                'themes'  => $this->_model->getThemes(),
                'filter' => $this->_model,
                'themeDropdownFilter' => function ($items=[], $options=[]) {
                    if (!$items) {
                        return;
                    }

                    return \yii\helpers\Html::activeDropDownList(
                        $this->_model,
                        ProductFilter::TYPE_THEME,
                        ArrayHelper::map(
                            $this->getItems($items),
                            'id',
                            function ($arr) {
                                if ($arr['depth'] === 0) {
                                    return $arr['title'];
                                }

                                return "=> " . $arr['title'];
                            }
                        ),
                        $options
                    );
                },
                'regionDropdownFilter' => function ($items=[], $options=[]) {
                    if (!$items) {
                        return;
                    }

                    return \yii\helpers\Html::activeDropDownList(
                        $this->_model,
                        ProductFilter::TYPE_REGION,
                        ArrayHelper::map(
                            $this->getItems($items),
                            'id',
                            function ($arr) {
                                if ($arr['depth'] === 0) {
                                    return $arr['title'];
                                }

                                return "=> " . $arr['title'];
                            }
                        ),
                        $options
                    );
                },
                'hotelstarsDropdownFilter' => function (
                    $options=[],
                    $stars=["3" => '3 Sterne',"3.5" => '3½ Sterne',"4" => '4 Sterne',"4.5" => '4½ Sterne',"5" => '5 Sterne']
                ) {
                    return \yii\helpers\Html::activeDropDownList(
                        $this->_model,
                        'hotelstars',
                        $stars,
                        $options
                    );
                }
            ],
            $this
        );
    }

    private $buffer = [];

    /**
     * merge parent and childs as flat array
     */
    public function getItems(array $items, $childkey='sub', $depth=-1): array
    {
        $depth++;
        $buffer = [];
        foreach ($items as $key => $item) {
            $item['depth'] = $depth;

            if (!isset($item[$childkey])) {
                $buffer[] = $item;
                continue;
            }

            $parent = $item;
            unset($parent[$childkey]);
            $buffer[] = $parent;

            foreach ($this->getItems($item[$childkey], $childkey, $depth) as $recursiveItem) {
                $buffer[] = $recursiveItem;
            }
        }

        return $buffer;
    }
}
