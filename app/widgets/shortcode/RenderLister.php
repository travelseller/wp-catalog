<?php

declare(strict_types=1);

namespace ts\catalog\widgets\shortcode;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use kartik\detail\DetailView;
use ts\catalog\models\ProductLister;
use ts\catalog\models\WpPost;

/**
 * Displays Tabs in Backend for Setup an IBE with jquery ui.
 * Each Tab contains a Pjax-Container with a ActiveForm,
 * the FormFields are rendered via Model::formFields() Method.
 */
class RenderLister extends Widget
{
    /**
     * @var int WP PostId
     */
    public $postId;

    /**
     * the name of the query-param to use
     */
    public $filterKey;

    /**
     * @var array default Filter Settings
     */
    public $filter = [];

    /**
     * @var array Filter-Params submitted by Request
     */
    protected $requestFilter;

    /**
     * @var array HTML-options
     * @see \yii\helpers\Html
     */
    public $options = [];

    /**
     * @var bool whether this lister should be shown only
     * if a Filterkey is present in current Request.
     */
    public ?bool $showOnFilter = null;

    /**
     * @var string named ModelClass to use as Endpoint.
     */
    public $endpointClassName;

    /**
     * @var int shortcode defined pagesize overrides Config-Defined one
     * if setted (in shortcode), pager will be enabled at all.
     */
    public $pagerSize;

    public $randomize = false;

    public $themeBlacklist = [];

    public $regionBlacklist = [];

    /**
     * @var \yii\data\ArrayDataProvider
     */
    private $dp;

    /**
     * @var ProductLister
     */
    private $lister;

    private $itemsId;

    public array $sort = [];

    public function init()
    {
        $this->lister = ProductLister::find()->andWhere(['pid' => $this->postId])->one();

        if (!$this->lister) {
            throw new \yii\base\InvalidConfigException('No ProductLister for postId (pid) ' . $this->postId . ' found!');
        }

        $filterKey = $this->filterKey ?? $this->lister->filter->filterKey ?? $this->lister->formName();
        $wpQuery = get_query_var($filterKey, []);
        $this->requestFilter = array_merge(
            Yii::$app->request->get($filterKey, !is_array($wpQuery) ? [$wpQuery] : $wpQuery),
            Yii::$app->request->post($filterKey, [])
        );

        $parms = array_merge(
            $this->filter,
            $this->requestFilter
        );

        $endpoint = null;
        if ($this->endpointClassName) {
            $endpoint = Yii::createObject($this->endpointClassName);
        }

        $this->dp = $this->lister->filter->search($parms, $endpoint);

        $models = $this->dp->allModels;

        if ($this->themeBlacklist) {
            $models = array_filter(
                $models,
                fn ($model) => !array_intersect(
                    $this->themeBlacklist,
                    ArrayHelper::getValue($model->getRaw(), 'themes', []) ?? []
                )
            );
        }

        if ($this->regionBlacklist) {
            $models = array_filter(
                $models,
                fn ($model) => !array_intersect(
                    $this->regionBlacklist,
                    ArrayHelper::getValue($model->getRaw(), 'regions', []) ?? []
                )
            );
        }

        if ($this->lister->randomize || $this->randomize === true) {
            shuffle($models);
        }

        $this->dp->setModels($models);

        if ($this->pagerSize || $this->lister->usePager) {
            $this->dp->setPagination(new Pagination([
                'pageSize' => $this->pagerSize ?? $this->lister->pagerSize,
                'pageParam' => $this->lister->pagerParam ?? 'tsIbePage'
            ]));
        } else {
            $this->dp->setPagination(false);
        }

        if ($this->sort) {
            $this->dp->setSort([
                'class' => \yii\data\Sort::class,
                'defaultOrder' => $this->sort,
                'attributes' => array_keys($this->sort)
            ]);
        }

        $this->dp->prepare(true);
    }

    protected function extractDataAttributes(string $input=null): array
    {
        if (!$input) {
            return [];
        }

        $dataOptions = [];
        foreach (explode(",", $input ?? '') as $option) {
            $pair = explode(":", $option);
            if (isset($pair[0]) && isset($pair[1])) {
                $dataOptions[$pair[0]] = $pair[1];
            }
        }

        return $dataOptions;
    }

    protected function getItemsId(): string
    {
        if (!$this->itemsId) {
            $this->itemsId = (new ListView([
                'dataProvider' => $this->dp,
            ]))->getId();
        }
        return $this->itemsId;
    }

    /**
     * @param string $template @ts/catalog/views/customs/35.php
     */
    public function renderItems(string $template, array $options=[]): string
    {
        global $wp;

        $cart = $this->lister->config->cartSettings->getCart(Yii::$app->session->id);
        $items = ListView::widget(ArrayHelper::merge([
            'id' => $this->getItemsId(),
            'dataProvider' => $this->dp,
            'layout' => '{items}',
            'options' => [
                'class' => ArrayHelper::getValue($this->options, 'class') ?? $this->lister->moduleCssClasses ?? 'list-view',
                'data' => $this->extractDataAttributes($this->lister->listerContainerDataAttributes)
            ],
            'itemOptions' => ['class' => $this->lister->moduleCssItemClasses ?? 'col'],
            'itemView' => function ($product, $key, $index, $widget) use ($wp, $cart, $template) {
                $product_id = $product->getPrimaryKey();
                $detailLink = $this->lister->getDetailLink($product_id);

                return $widget->render(
                    $template,
                    array_merge([
                        'index' => $index,
                        'key' => $key,
                        'detailLink' => $detailLink,
                        'addToCartLink' => function ($title, $qty, $htmlOptions=[], $sku=null, $productOptions=[]) use ($cart, $product) {
                            ArrayHelper::setValue($htmlOptions, 'data', $cart->getAddLinkParms($this->lister->config->cartSettings, $product, $qty, $sku, $productOptions));
                            return Html::a($title, '', $htmlOptions);
                        },
                        'removeFromCartLink' => function ($linkTitle, $htmlOptions=[], $sku=null) use ($cart, $product) {
                            ArrayHelper::setValue($htmlOptions, 'data', $cart->getRemoveLinkParms($this->lister->config->cartSettings, $product, $sku));
                            return Html::a($linkTitle, '', $htmlOptions);
                        },
                        'raw' => $product->getRaw(),
                        'product' => $product,
                        'cart' => $cart,
                        'lister' => $this->lister,
                        'totalCount' => $this->dp->getTotalCount()
                    ], $product->getRaw())
                );
            }
        ], $options));

        return $items;
    }

    public function run()
    {
        if (
            ($this->lister->hideWhenReaderActive && Yii::$app->getProductIdSlug($this->lister->config->reader))
            || (!$this->requestFilter && $this->showOnFilter === true)
            || ($this->requestFilter && $this->showOnFilter === false)
        ) {
            echo '';
            return;
        }

        $items = $this->renderItems($this->lister->itemTemplate->filePath);

        /**
         * For Summary and Pager
         */
        $listView = new ListView([
            'dataProvider' => $this->dp,
            'pager' => [
                // Container Options
                'options' => ['class' => $this->lister->pagerContainerCssClasses],
                // Item Options
                'linkContainerOptions' => ['class' => $this->lister->pagerLinkContainerCssClasses],
                // ahref options
                'linkOptions' => [
                    'class' => $this->lister->pagerLinkCssClasses,
                    'data' => $this->extractDataAttributes($this->lister->pagerLinkDataAttributes),
                ]
            ]
        ]);

        $pager = $this->lister->usePager ? $listView->renderSection('{pager}') : null;
        $summary = $listView->renderSection('{summary}');

        $view = $this->getView();
        $view->registerAssetBundle(\ts\catalog\CatalogAssetBundle::class);
        $cart = $this->lister->config->cartSettings->getCart(Yii::$app->session->id);

        $r = $view->render(
            $this->lister->template->filePath,
            [
                'widget' => $this,
                'items' => $items,
                'dp' => $this->dp,
                'items_container_id' => $this->getItemsId(),
                'pager' => $pager,
                'summary' => $summary,
                'filter' => $this->lister->filter,
                'cart' => $cart,
            ]
        );
        echo $r;
    }
}
