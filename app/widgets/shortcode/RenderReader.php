<?php

declare(strict_types=1);

namespace ts\catalog\widgets\shortcode;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use kartik\detail\DetailView;
use ts\catalog\models\ProductReader;

/**
 * Displays Tabs in Backend for Setup an IBE with jquery ui.
 * Each Tab contains a Pjax-Container with a ActiveForm,
 * the FormFields are rendered via Model::formFields() Method.
 */
class RenderReader extends Widget
{
    /**
     * @var int WP PostId
     */
    public $postId;

    /**
     * @var ProductReader
     */
    private $reader;

    public function init()
    {
        $this->reader = ProductReader::find()->andWhere(['pid' => $this->postId])->one();

        if (!$this->reader) {
            throw new \yii\base\InvalidConfigException('No ProductReader for postId (pid) ' . $this->postId . ' found!');
        }
    }

    public function run()
    {
        $productIdSlug = Yii::$app->getProductIdSlug($this->reader);
        $cart = $this->reader->config->cartSettings->getCart(Yii::$app->session->id);
        $view = $this->getView();
        $view->registerAssetBundle(\ts\catalog\CatalogAssetBundle::class);

        if (
            !$productIdSlug
            || !($product = $this->reader->config->endpoint->findOne($productIdSlug))
            || !$product->id
        ) {
            if ($this->reader->errorTemplate) {
                echo $view->render($this->reader->errorTemplate->filePath);
            }
            return;
        }

        echo $view->render(
            $this->reader->template->filePath,
            array_merge([
                'raw' => $product->getRaw(),
                'product' => $product,
                'reader' => $this->reader,
                'cart' => $cart,
                'removeFromCartLink' => function ($linkTitle, $htmlOptions=[], $sku=null) use ($cart, $product) {
                    ArrayHelper::setValue($htmlOptions, 'data', $cart->getRemoveLinkParms($this->reader->config->cartSettings, $product, $sku));
                    return Html::a($linkTitle, '', $htmlOptions);
                },
                'addToCartLink' => function ($title, $qty, $htmlOptions=[], $sku=null, $productOptions=[]) use ($cart, $product) {
                    ArrayHelper::setValue($htmlOptions, 'data', $cart->getAddLinkParms($this->reader->config->cartSettings, $product, $qty, $sku, $productOptions));
                    return Html::a($title, '', $htmlOptions);
                },
            ], $product->getRaw())
        );
    }
}
