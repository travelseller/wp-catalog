<?php

declare(strict_types=1);

namespace ts\catalog\widgets\tabs;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;

/**
 * Displays Tabs in Backend for Setup an IBE with jquery ui.
 * Each Tab contains a Pjax-Container with a ActiveForm,
 * the FormFields are rendered via Model::formFields() Method.
 */
class PostTabs extends Widget
{
    public function init()
    {
        parent::init();

        global $post;

        wp_enqueue_style(
            'tscatalog_tabs_stylesheet',
            plugins_url('travelseller-catalog/app/widgets/tabs/assets/tabs.css')
        );

        wp_enqueue_style('jquery-ui');

        wp_enqueue_script(
            'tscatalog-admin-tab-script',
            plugins_url('travelseller-catalog/app/widgets/tabs/assets/tabs.js'),
            [
                'jquery','jquery-ui-tooltip','jquery-ui-tabs'
            ]
        );

        Yii::$app->modelRegistry
            // Process only current post type
            ->filterPostType($post->post_type)
            // populate models
            ->findByPostId($post->ID)
            // validate again to show invalid data: persist cylce has redirected.
            ->validate();
    }

    protected function getIsNewRecord(): bool
    {
        global $pagenow;

        return ($pagenow == 'post-new.php');
    }

    public function run()
    {
        $out = Html::beginTag('div', [
            'id' => 'tscatalog_settings_tabs_wrapper',
        ]);

        $out .= Html::beginTag('ul', [
            'id' => 'tscatalog_settings_tabs',
        ]);

        foreach (Yii::$app->modelRegistry->get() as $model) {
            $out .= $this->renderTabListItem($model);
        }

        $out .= Html::endTag('ul');

        foreach (Yii::$app->modelRegistry->get() as $model) {
            $out .= $this->renderTabContent($model);
        }

        $out .= Html::endTag('div');

        echo $out;
    }

    protected function renderTabListItem(\yii\base\Model $model): string
    {
        $out = Html::beginTag('li');

        $href = '#' . crc32(get_class($model)) . '-tab';
        $out .= Html::a(
            '<span>'. Yii::t('tsCatalog', get_class($model)) .'</span>',
            $href,
            ['id' => 'tab-' . $model->formName()]
        );
        $out .= Html::endTag('li');

        return $out;
    }

    protected function renderTabContent($model): string
    {
        $cssId = crc32(get_class($model)) . '-tab';

        $out = Html::beginTag('div', [
            'id' =>$cssId,
            'class' => 'tscatalog-tab'
        ]);

        // add css class for Input-errors
        $attributes = $model->formFields();

        if ($model->hasErrors()) {
            foreach ($attributes as $key => $attribute) {
                if (
                    !isset($attribute['attribute'])
                    || !$model->hasErrors($attribute['attribute'])) {
                    continue;
                }

                $options = ArrayHelper::getValue($attributes, $key . '.options', []);
                Html::addCssClass($options, ['ui-state-error','ui-corner-all']);
                ArrayHelper::setValue($attributes, $key . '.options', $options);
            }
        }

        $out .= DetailView::widget([
            'formClass' => ActiveForm::class,
            'model' => $model,
            'attributes' => $attributes,
            'mode' => DetailView::MODE_EDIT,
            'bootstrap' => false,
            'mainTemplate' => '{detail}',
            'hAlign' => 'left',
            'options' => ['style' => 'width:100%']
        ]);

        $out .= Html::endTag('div');

        return $out;
    }
}
