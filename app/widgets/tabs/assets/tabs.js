(function($) {
    $( document ).ready( function() {
    	var selectedTabId = sessionStorage.getItem("tsCatalogSelectedTab");
	    selectedTabId = selectedTabId === null ? 0 : selectedTabId;
	    $("#tscatalog_settings_tabs_wrapper").tabs({
	        active: selectedTabId,
	        activate : function( event, ui ) {
	            selectedTabId = $("#tscatalog_settings_tabs_wrapper").tabs("option", "active");
	            sessionStorage.setItem("tsCatalogSelectedTab", selectedTabId);
	        }
	    });
        $( '.tscatalog-tab' ).css( 'min-height', $( '#tscatalog_settings_tabs' ).css( 'height' ) );
    });
})(jQuery);