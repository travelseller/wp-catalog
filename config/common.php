<?php

return [
    'id' => 'tsIbe',
    'aliases' => [
        '@ts/catalog' => dirname(__DIR__) . '/app',
        '@log' => dirname(dirname(dirname(__DIR__))),
        '@app' => dirname(__DIR__) . '/app',
    ],
    'basePath' => dirname(__DIR__),
    'language' => 'de',
    'controllerMap' => [
        'build' => "ts\catalog\console\BuildController"
    ],
    'bootstrap' => ['log'],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cacheFileSuffix' => 'wpTsIbe',
            'defaultDuration' => 600,
        ],
        'modelRegistry' => [
            'class' => 'ts\catalog\servicelocators\ModelRegistry',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset'  => DB_CHARSET,
            'dsn' => 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME,
            'tablePrefix' => $GLOBALS['table_prefix']
        ],
        'i18n' => [
            'translations' => [
                'ts*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'forceTranslation' => true,
                ]
            ]
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error','warning','trace'],
                    'logFile' => '@log/debug.log',
                    'logVars' => [],
                ],
            ],
        ],
    ],
];
