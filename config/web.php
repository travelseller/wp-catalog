<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'params' => [
        'bsVersion' => 4
    ],
    'controllerMap' => [
        'rest' => 'ts\catalog\controllers\RestController'
    ],
    'components' => [
        'view' => [
            'class' => '\ts\catalog\components\View',
        ],
        'request' => [
            'cookieValidationKey' => '6ypiH9TeAc9U8ju6PMs24nA39Scyea1I',
        ],
        'assetManager' => [
            'basePath' => '@app/assets',
            'baseUrl' => '@web/wp-content/plugins/travelseller-catalog/app/assets',
            'bundles' => [
                'yii\bootstrap4\BootstrapAsset' => [
                    'css' => [],
                ],
                'yii\bootstrap4\BootstrapPluginAsset' => [
                    'js' => []
                ]
            ]
        ],
    ],
];
