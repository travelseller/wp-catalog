# Wordpress Installation

get the latest [Release](http://codebase.travelseller.net:3000/ts/travelseller-catalog/releases) and copy the content of the Compressed Package to `wp-content/plugins`. The Structure of plugins-folder should contain at least:

	wp-content/plugins
	 |- travelseller-catalog
	 	|- app
	 	|- config
	 	|- runtime
	 	|- vendor
	 	|- travelseller-catalog.php

Head to the WordPress `Backend -> Plugins`, you should see the Travelseller Catalog Plugin. Click `activate` to activate the Plugin.

# Create a Build from Master

1. clone the repo with `git clone http://codebase.travelseller.net:3000/ts/travelseller-catalog.git`
2. run `composer update` inside the of `travelseller-catalog` Folder
3. Remove `.git` Folder

composer create project ts/