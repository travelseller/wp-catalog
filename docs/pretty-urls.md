# Pretty URLs

### Rewrite Regel erstellen

1. gewünschte post-id des Readers (Seite mit Reader-shortcode) bereithalten
2. einen [Slug](https://de.wikipedia.org/wiki/Clean_URL) aussuchen.

Im nachfolgenden Bsp. wird die Seite mit der ID 139 und der Slug `reise/reisename` verwendet.
Demnach sind Reisen mit dem Schema `reisefoo.de/reise/10-Tage-Bitterfeld` erreichbar.

Transaktionspfad: `Design -> Theme-Editor -> Theme-Funktionen (functions.php)`

```php
<?php 

add_action('init', function () {
    add_rewrite_tag('%reise%', '([a-z0-9\-]+)');
    // page_id=139 muss entsprechend angepasst werden: hier die page_id eintragen, welche den shortcode [render_tsCatalog_reader]
    // enthält
    add_rewrite_rule('^reise/([^/]+)/?$', 'index.php?page_id=139&reise=$matches[1]', 'top');
});

// ...
```

*!Rewrite-Änderungen werden erst nach erneutem speichern der Permalink-Einstellungen wirksam!*


### Katalog Anfrage-Parameter anpassen


Transaktionspfad: `TS-Catalog -> Catalog Settings -> bearbeiten -> Detailansicht`


Anfrageparameter: reise


### Wordpress permalink-struktur anpassen

Transaktionspfad: `Einstellungen -> Permalinks ->  Individuelle Struktur`

Wert: `/%postname%/%reise%`


### Verwendung im Lister-Template

```php
<a href="/reise/<?=$link;?>">zum Angebot</a>
```

