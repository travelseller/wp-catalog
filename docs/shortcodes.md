# Shortcodes

Eine Liste verfügbarer Shortcodes findet sich unter: `TS-Catalog -> Catalog Settings` (Spalte "shortcode").
Der Catalog stellt diese Shortcodes zur Verfühung:

## Auflistung ("lister")

gibt eine Reiseliste aus.

```
[render_tsCatalog_lister id=1]
```

### Parameter


`class`: setzt CSS-Klassen
```   
[render_tsCatalog_lister id=1 class=foo bar baz] 
```

`pager`: konfiguriert die Anzahl der Ergebnisse je Seite

```   
[render_tsCatalog_lister id=1 pager=3] 
```

`endpoint`: definiert das Datenmodel und den zugrundeliegenden API-Endpunkt, mögliche Werte:

- ts\catalog\models\StandardProduct
- ts\catalog\models\HotelProduct
- ts\catalog\models\ServiceProduct

```   
[render_tsCatalog_lister id=1 endpoint=ts\catalog\models\StandardProduct]
```

`showonfilter`: steuert, ob dieser Shortcode in Kombination mit einem Filter angezeigt werden soll.

```
[render_tsCatalog_lister id=1 showonfilter=true]
[render_tsCatalog_lister id=1 showonfilter=false]
```

- Wert "true": Shortcode wird ignoriert, wenn kein Filter gesetzt ist
- Wert "false": Shortcode wird ignoriert ausgeblendet, wenn ein Filter gesetzt ist



`themeblacklist`: Thema-Ids, welche ausgeschlossen werden solln

```
[render_tsCatalog_lister id=1 themeblacklist=1001,2002,3003]
```

`regionblacklist`: Region-Ids, welche ausgeschlossen werden solln

```
[render_tsCatalog_lister id=1 regionblacklist=1001,2002,3003]
```

`sort`: konfiguriert Sortierung


```
[render_tsCatalog_lister id=1 sort=title:3]
[render_tsCatalog_lister id=1 sort=title:4]
[render_tsCatalog_lister id=1 sort=price:4]
```

- Wert "title:3": Liste wird nach Attribut "title" absteigend sortiert
- Wert "title:4": Liste wird nach Attribut "title" aufsteigend sortiert
- Wert "price:4": Liste wird nach Attribut "price" aufsteigend sortiert
    
Alle verfügbaren Attribute finden sich [hier](https://app.swaggerhub.com/apis/travelseller/TravelsellerRestAPI/1.3#/ProductShortInfo)


`filterkey`: legt den Anfrageparameter fest


```
[render_tsCatalog_lister id=1 filterkey=suchfilter]
```

`http://foo-bar.de/alle-reisen.html?suchfilter=...` 


```
[render_tsCatalog_lister id=1 filterkey=supersearch]
```

`http://foo-bar.de/alle-reisen.html?supersearch=...` 


## Filter

[render_tsCatalog_filter id=1]

keine weiteren Konfigurationen möglich


## Produktdetails ("reader")


[render_tsCatalog_reader id=1]

keine weiteren Konfigurationen möglich