Travelseller Catalog - Wordpress plugin
=======================================

Das Katalog-Plugin verbindet mit der Travelseller-API und überträgt Reiseangebote, welche dann auf der Webseite dargestellt werden können.
Jede API-Anfrage wird auf dem Webserver der Wordpress-Instanz zwischengespeichert (cache).
Eine Beschreibung aller API-Endpunkte ist im [swaggerhub](https://app.swaggerhub.com/apis/travelseller/TravelsellerRestAPI/1.2) verfügbar.

- [installation guide](docs/installation.md)
- [pretty urls](docs/pretty-urls.md)
- [shortcodes](docs/shortcodes.md)


## Neue API-Verbindung hinzufügen

Transaktionspfad: `Travelseller Catalog -> Add new`

1. Titel: `<zeichenkette>` = der Titel der Verbindung z.B: `Test-Verbindung`

2. Endpoint Url: `<zeichenkette>` = https://showcase.travelseller.net/restapi
(`showcase` muss mit Ihrem Mandanten ersetzt werden)

3. Api Key: `<zeichenketter>` = die API-Key erhalten Sie von Travelseller

4. Brand id: `<zeichenketter>` = die Brand id erhalten Sie von Travelseller

5. klicken Sie `speichern`


## Katalog Einstellungen

es können mehrere Einstellungen für die selbe API-Verbindung angelegt werden. Eine `Einstellung` umfasst 

- Einstellungen (Caching Strategie)
- Auflistungen (z.B. Template, Css-Klassen, Pagination und Anzahl der Ergebnisse)
- Filter (z.B. Weiterleitung nach Filterung, Template, Query-Paramter)
- Detailansicht (Template und Anfrageparameter)

Es müssen nicht alle Einstellungen vorgenommen werden, soll z.B. eine Einstellung `3 Produkte auf Startseite` lediglich 3 Produkte auflisten, dann muss nur die Einstellung `Auflistung` vorgenommen werden, der Rest kann ignoriert werden.

Jede Einstellung kann dann als [Shortcode](https://www.ionos.de/digitalguide/hosting/blogs/wordpress-shortcodes-die-nuetzlichen-kurzbefehle/) in eine Wordpress-Seite eingefügt werden.

Mehr zu den einzelnen Einstellungen im Nachfolgenden.

### Cache Art

- File Cache: benutzt das Filesystem als Cache-Adapter

- Redis Cache: benutzt Redis als Cache-Adapter

### Auflistung

eine Auflistung zeigt Reiseprodukte als Liste an. Es werden 2 Templates implementiert: das Modultemplate und das Itemtemplate.
Ein Template ist eine php-Datei im Dateisystem der Wordpress-Instanz. Das Template enthält je nach Templateart unterschiedliche Variablen. Detailierte Informationen erhalten Sie in der [Template](#template)-Sektion.

Dieser HTML-Code beschreibt beide Templates:

Das Modultemplate dient als Wrapper für die Itemtemplates.
Das Itemtemplate wird für jede Reise erneut gerendert:

```html
<!-- Start Modultemplate -->
<div class="row mx-0" data-foo="bar" data-baz="bar">
	<!-- Start Itemtemplate Produkt 1 -->
	<div class="col-4">
		<div><!-- Bild --></div>
		<h2><!-- Reisetitel --></h2>
		<p><!-- Reisekurzbeschreibung --></h2>
	</div>
	<!-- Ende Itemtemplate -->

	<!-- Start Itemtemplate Produkt 2-->
	<div class="col-4">
		<div><!-- Bild --></div>
		<h2><!-- Reisetitel --></h2>
		<p><!-- Reisekurzbeschreibung --></h2>
	</div>
	<!-- Ende Itemtemplate -->

	<!-- ... weiter für alle Reisen in der Auflistung ... -->

</div>
<!-- Ende Modultemplate -->
```

#### Layout Einstellungen

1. Weiterleitung: wohin bei Klick auf ein Reiseangebot weiterleiten?

2. Modultemplate: welches Template als Modultemplate verwenden?

3. Container Css Klassen: CSS-Klassen für das Modultemplate z.B. "row mx-0"

4. Container Item Cass Klassen: CSS-Klassen für das Itemtemplate z.B. "col-4"

5. Lister Data-Attributes: Data-Attribute für das Modultemplate, z.B. "foo:bar,baz:bar" wird als `data-foo="bar" data-baz="bar"` ausgegeben.

6. Itemtemplate: Das zu verwendene Itemtemplate

7. bei Detailansicht ausblenden: soll das Auflistungsmodul ausgeblendet werden, wenn eine Detailansicht aktiv ist?

8. zufällige Sortierung: sortiert die Auflistung zufällig


#### Pagination Einstellungen
(Achten Sie darauf, dass im Modultemplate der Tag `<?=$pager;?>` eingefügt ist.)


Die Pagination wird als ungeordnete Liste (`<ul>`) ausgegeben, Bsp:

```html
<ul class="foo">
	<li class="bar active">
		<a class="baz" href="https://...../?show-page=1" data-foo="bar" data-page="0">1</a>
	</li>
	<li class="bar">
		<a class="baz" href="https://...../?show-page=2" data-foo="bar" data-page="1">2</a>
	</li>
</ul>
```

1. Container Css Klassen: Css-Klassen des `<ul>-Tags`, z.B. `foo`

2. Linkcontainer Css Klassen: Css-Klassen des `<li>-Tags`, z.B. `bar`

3. Link Css Klassen: Css-Klassen des `<a>-Tags`, z.B. `baz`

4. Link Data-Attribut: Data-Attribute des `<a>-Tags`, z.B. `foo:bar` wird als `data-foo="bar"` ausgegeben.

5. Anzahl Ergebnisse pro Seite: Anzahl Ergebnisse pro Seite

6. Queryparameter: der zu verwendene Anfrageparamter, z.B. `show-page`, Standard ist `tsIbePage`


### Filter

Der Zweck der Filter-Komponente ist die Filterung von Produkt-Auflistungen. Sie kann als Navigation-Menü im Wordpress-Customizer (Design -> Customizer) oder als [Shortcode](https://www.ionos.de/digitalguide/hosting/blogs/wordpress-shortcodes-die-nuetzlichen-kurzbefehle/) an beliebiger Stelle hinzugefügt werden.

Im wesentlichen konstruiert der Filter Links zu einer Auflistung und setzt Query-Parameter für themeid und regionid.

#### Layout Einstellungen

1. Weiterleitung: zu welcher Auflistungs-Seite soll der Filter weiterleiten?

2. Modultemplate: welches Template als Modultemplate verwenden?

3. Anfrageparameter: der zu verwendene Queryparameter (standard ist `ProductFilter`)

4. Vorgabe Filter: Wenn kein Filter durch den Webseitenbesucher gesetzt ist, werden diese Filtereinstellungen verwendet. Z.B.: `regionid=1000,1110,22201` filtert nach regionen mit den angegeben ids.

5. bei Detailansicht ausblenden: soll das Filtermodul ausgeblendet werden, wenn eine Detailansicht aktiv ist?

#### Navigation Settings

1. Nav Filter Type: configures, which Attributte on API-Endpoint should be called. Refer to https://app.swaggerhub.com/apis/travelseller/TravelsellerRestAPI/1.2#/catalog/get_getOffers for more informations.

2. Navigationstemplate: the template to use for navigation rendering

3. Nav Lvl1 as Menuitem: whether the first element should be used as Navigation-Title.

### Detailansicht

Die Detailanischt zeigt ein Produkt im Detail.

1. Modultemplate: welches Template als Modultemplate verwenden?

2. Anfrageparameter: der zu verwendene Queryparameter (standard ist `tsIbeOffer`)

## Templates

Jedem Template werden alle Variablen welche der jeweilige API-Endpunkt zurückgibt zugewiesen.
Der gesamte raw-stack wird jedem Template als Variable`$raw` zugewiesen.

Travelseller-Catalog wurde mit dem Yii2-Framework umgesetzt. Demnach sind alle Yii2 Komponenten in den Templates verfügbar.

Sinnvolle Helfer sind u.a.:

- [Formatter](https://www.yiiframework.com/doc/api/2.0/yii-i18n-formatter)
- [ArrayHelper](https://www.yiiframework.com/doc/api/2.0/yii-helpers-arrayhelper)
- [StringHelper](https://www.yiiframework.com/doc/api/2.0/yii-helpers-stringhelper)

## Beispiele

### 4 Produkte auf Startseite mit Link zur Detailansicht anzeigen

- neue Wordpress-Seite "Startseite" anlegen und als Standardseite festlegen
- neue Wordpress-Seite "Reisedetails" anlegen

#### Templates

1. neues Template `mod_lister_container`:

```html
<div class="container">
	<?=$items;?>
</div>
```

2. neues Template `item_lister_startpage`:

```html
<a class="text-decoration-none bg-white d-block" href="<?=$detailLink;?>">
	<?php $pic_1_url = (isset($pictures[0]) && isset($pictures[0]['media_URL']) ? $pictures[0]['media_URL'] : null);?>
	<div class="row">
		<div class="position-relative col-6 col-lg-12">
			<?php if($pic_1_url):?>
				<div class="overflow-hidden">
					<img src="<?=$pic_1_url;?>___newx__255___newy__255" class="img-fluid">
				</div>
			<?php endif;?>

			<div class="position-absolute rounded-right text-white bg-danger p-2" style="bottom: 30px">
				<h3 class="mb-0"><?=$price;?><span class="ml-2" style="font-size: 40%">p.P.</span></h3>
			</div>
		</div>

		<div class="col-6 col-lg-12">
			<div class="p-lg-3 teaser-content">
				<p class="font-weight-bold text-black"><?=($title ?? '');?></p>
				<p class="text-secondary"><?=$services;?></p>
			</div>
			<div class="border-top pt-3 pb-3 w-100 text-danger text-sans-condensed font-weight-bold" style="border-bottom: 2px solid #dee2e6">Zum Arrangement</div>
		</div>
	</div>
</a>
```

3. neues Template `mod_detailansicht`

```html
<h1><?=$title;?></h1>

<h2>Beschreibung:</h2>

<?=$text_1;?>
<?=$text_2;?>
<?=$text_3;?>
```


#### Neue Katalog Catalog Setting:

##### Einstellungen

- Titel: "Startpage 4 Product-Items"
- Verbindung: eine beliebige Verbindung wählen
- Cache Art: File Cache

##### Auflistung

- Weiterleitung: "Reisedetails"
- Modultemplate: "mod_lister_container"
- Container Css Klassen: "row mx-0"
- Container Item Css Klassen: "col-lg-3 col-sm-12 mb-4"
- Itemtemplate: "item_lister_startpage"
- Pagination verwenden? "Ja"
- Anzahl Ergebnisse pro Seite: "4"
- Queryparameter: "tsIbePage"

##### Detailansicht

- Modultemplate: "mod_detailansicht"

speichern/veröffentlichen


#### Shortcode einfügen

ein Klick auf "TS-Catalog -> Catalog Settings" zeigt eine Übersicht aller vorhandenen Katalogeinstellungen.
Die Spalte "shortcode" enthält den entsprechenden Shortcode für jedes Modul (lister,filter und reader).

Der Shortcodes für die Einstellung `Startpage 4 Product-Items` müssen wie folgt eingesetzt werden:

- `[render_tsCatalog_lister ...`  muss auf die Startseite eingesetzt werden,
- `[render_tsCatalog_reader ...`  muss in die Reisedetails-Seite eingesetzt werden