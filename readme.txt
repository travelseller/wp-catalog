=== travelseller catalog ===
Contributors: Henry Volkmer, Travelseller GmbH
Requires at least: 4.6

Travelseller Catalog Plugin

== Description ==

Bring all your Products and Accommodation live to your WordPress Site.
