<?php
/**
 * @package TS_Catalog
 * @version 1.0
 */
/*
Plugin Name: TS-Catalog
Description: TS-Catalog brings your Travelproducts to your WordPress-Site.
Version: 1.0
*/

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use ts\catalog\widgets\tabs\PostTabs;
use ts\catalog\models\Connection;
use ts\catalog\models\Config;
use ts\catalog\models\ProductLister;
use ts\catalog\models\ProductReader;
use ts\catalog\models\ProductFilter;
use ts\catalog\models\CartSettings;
use ts\catalog\models\Cart;
use ts\catalog\models\Template;
use ts\catalog\widgets\shortcode\RenderReader;
use ts\catalog\widgets\shortcode\RenderLister;
use ts\catalog\widgets\shortcode\RenderFilter;
use ts\catalog\widgets\shortcode\RenderCart;
use ts\catalog\widgets\cart\CartWidget;

require_once(ABSPATH . 'wp-includes/pluggable.php');
require_once(ABSPATH . 'wp-admin/includes/plugin.php');

if (!class_exists('Yii')) {
    require __DIR__ . '/vendor/autoload.php';
    require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';
}

Yii::setAlias('@config', __DIR__ . '/config');

$config = ArrayHelper::merge(
    require Yii::getAlias('@config/web.php'),
    require Yii::getAlias('@config/common.php')
);

(new \ts\catalog\Application($config));

if (!defined("WP_DEBUG") || !WP_DEBUG) {
    Yii::$app->errorHandler->unregister();
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'prod');
} elseif (WP_DEBUG === true) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
}

if (!Yii::$app->session->isActive) {
    Yii::$app->session->open();
}


add_action('admin_enqueue_scripts', function ($hook) {
    if (
        !WP_DEBUG
        || tsCatalog_isFrontend()
        || (
            'edit.php' !== $hook
            && Yii::$app->request->get('post_type') !== 'tsdebuglog'
        )
    ) {
        return;
    }
    Yii::$app->assetManager->baseUrl = Yii::getAlias('@web/../wp-content/plugins/travelseller-catalog/app/assets');
    $view = Yii::$app->getView();
    $view->registerAssetBundle(ts\catalog\CatalogAssetBundle::class);
    $view->registerAssetsToWp();
});

if (WP_DEBUG && is_admin() && Yii::$app->request->get('tsq')) {
    Yii::$app->controllerMap['debug'] = ts\catalog\controllers\DebugController::class;
    Yii::$app->runAction(Yii::$app->request->get('tsq'));
}

/**
 * register assets
 */

function ts_register_catalogassets()
{
    $view = Yii::$app->getView();
    $view->registerAssetBundle(ts\catalog\YiiAsset::class);
    $view->registerAssetsToWp();
}
add_action('wp_enqueue_scripts', 'ts_register_catalogassets');


/**
 * catalog-related hooks
 */
if (file_exists(get_template_directory() . '/ts-catalog.php')) {
    require_once get_template_directory() . '/ts-catalog.php';
}

/**
 * Support for rank-math
 */
add_action('rank_math/vars/register_extra_replacements', function () {
    rank_math_register_var_replacement(
        'get_ts_catalog_product',
        [
            'name' => esc_html__('Get Catalog Product variable', 'rank-math'),
            'description' => esc_html__('Gets a Variable from current active product.', 'rank-math'),
            'variable' => 'get_ts_catalog_product',
            'example' => 'get_ts_catalog_product_callback()',
        ],
        'get_ts_catalog_product_callback'
    );
});
function get_ts_catalog_product_callback($readerIdAndKey)
{
    list($readerId, $key) = explode(",", $readerIdAndKey);

    $reader = ProductReader::find()->andWhere(['pid' => $readerId])->one();
    $productIdSlug = Yii::$app->getProductIdSlug($reader);
    if (empty($productIdSlug)) {
        return null;
    }

    $product = $reader->config->endpoint->findOne($productIdSlug);
    $res =  ArrayHelper::getValue($product->raw, $key);
    return $res;
}


/**
 * Handle Cart
 */
function ts_handleCart()
{
    if (
        is_admin()
        || !($action = Yii::$app->request->post('tsCartAction'))
        || !($cartSettings = CartSettings::findOne(Yii::$app->request->post('cartSettings')))
    ) {
        return;
    }

    switch ($action) {

        case "addItem":
            $sku = Yii::$app->request->post('sku');
            $options = [];
            if (Yii::$app->request->post('product_options')) {
                $options = Json::decode(base64_decode(Yii::$app->request->post('product_options')));
            }
            $options['id'] = (int) Yii::$app->request->post('product_id');

            $product = $cartSettings->config->endpoint->findOne($options);

            if ($product) {
                $qty = Yii::$app->request->post('qty', 1);

                $cart = $cartSettings->getCart(Yii::$app->session->id);

                /**
                 * if given product_id is allready existing as cartItem, sum up quanty
                 */
                if (($cartItem = $cart->getItemByProduct($product, $sku))) {
                    $cartItem->updateCounters(['qty' => $qty]);
                } else {
                    $cart->addItem($cartSettings, $product, $qty, $sku, $options);
                }

                if ($cartSettings->cartAddedText) {
                    Yii::$app->session->flash('cart_flash', $cartSettings->cartAddedText);
                }
            } else {
                Yii::$app->session->flash('cart_flash', 'unable to move your product to cart!');
            }

            if ($cartSettings->jumpTo) {
                $jumpTo = get_permalink($cartSettings->jumpTo);
                wp_redirect($jumpTo);
                exit;
            }
            break;

        case "removeItem":
            $cart = $cartSettings->getCart(Yii::$app->session->id);
            $cartItem = $cart->getItem((int) Yii::$app->request->post('cartItemId'));
            if ($cartItem) {
                $cart->removeItem($cartItem);
            }
            break;
    }
}

add_action('wp_loaded', 'ts_handleCart');

function ts_exposeRestApi()
{
    if (
        is_admin()
        || !Yii::$app->request->isAjax
        || !Yii::$app->request->get('config')
        || !Yii::$app->request->get('endpoint')
    ) {
        return;
    }
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $response = Yii::$app->runAction('rest/index');
    $response->send();
    exit();
}
add_action('wp_loaded', 'ts_exposeRestApi');


/**
 * register ActiveRecords here to handle CRUD Cycle automated.
 * - Models are registered to Tabs
 * - Models will be filled with $_POST-Data
 */
Yii::$app->modelRegistry->addMultiple([
    new Connection(),
    new Config(),
    new ProductLister(),
    new ProductFilter(),
    new ProductReader(),
    new CartSettings(),
    new Template()
]);



/**
 * WP-Hooks
 */

/**
 * Installation
 */
register_activation_hook(__FILE__, function () {
    $webApp = Yii::$app;
    new \yii\console\Application(require Yii::getAlias('@config/common.php'));
    Yii::$app->runAction('migrate/up', ['interactive' => false]);
    Yii::$app = $webApp;
});

register_uninstall_hook(__FILE__, 'tsCatalog_uninstall');

function tsCatalog_uninstall()
{
    Yii::$app->db->createCommand('DROP TABLE IF EXISTS {{%tsCatalog_configs}}')->execute();
    Yii::$app->db->createCommand('DROP TABLE IF EXISTS {{%tsCatalog_connections}}')->execute();
    Yii::$app->db->createCommand('DROP TABLE IF EXISTS {{%migration}}')->execute();
}

/**
 * Action links in Pluginpage
 */
add_filter('plugin_action_links', function ($links, $file) {
    if (basename($file) !== basename(__FILE__)) {
        return $links;
    }

    array_unshift(
        $links,
        \yii\helpers\Html::a(
            Yii::t('tsCatalog', 'add a connection'),
            'post-new.php?post_type=' . Connection::getWpType()
        )
    );

    return $links;
}, 10, 2);

register_post_type(Connection::getWpType(), [
    'labels' => [
        'name' => 'TS-Catalog Connection',
        'singular_name'  => 'connection',
        'add_new_item' => 'Add new connection',
        'edit_item' => 'Edit connection',
        'new_item' => 'New connection',
        'view_item' => 'View connection',
        'search_items' => 'Search connections',
        'not_found' => 'No connections found',
        'menu_name' => "TS-Catalog",
    ],
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search'   =>  true,
    'query_var'             =>  true,
    'has_archive'           =>  false,
    'rewrite'               =>  false,
    'supports'              =>  ['title', 'author', 'page-attributes'],
]);

register_post_type(ProductLister::getWpType(), [
    'labels' => [
        'name' => 'Catalog Setting',
        'singular_name'  => 'Catalog Setting',
        'add_new_item' => 'Add new Setting',
        'edit_item' => 'Edit Setting',
        'new_item' => 'New Setting',
        'view_item' => 'View Setting',
        'search_items' => 'Search Settings',
        'not_found' => 'No Settings found',
        'menu_name' => "Catalog Settings",
    ],
    'show_in_menu' => 'edit.php?post_type=' . Connection::getWpType(),
    'public' => true,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search'   =>  true,
    'query_var'             =>  true,
    'has_archive'           =>  false,
    'rewrite'               =>  false,
    'supports'              =>  ['title', 'author', 'page-attributes'],
]);

register_post_type(Template::getWpType(), [
    'labels' => [
        'name' => 'Template Setting',
        'singular_name'  => 'Template Setting',
        'add_new_item' => 'Add new Template',
        'edit_item' => 'Edit Template',
        'new_item' => 'New Template',
        'view_item' => 'View Template',
        'search_items' => 'Search Templates',
        'not_found' => 'No Templates found',
        'menu_name' => "Templates",
    ],
    'show_in_menu' => 'edit.php?post_type=' . Connection::getWpType(),
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search'   =>  true,
    'query_var'             =>  true,
    'has_archive'           =>  false,
    'rewrite'               =>  false,
    'supports'              =>  ['title', 'author', 'page-attributes'],
]);


register_post_type('tsdebuglog', [
    'labels' => [
        'name' => 'show debuglog',
        'singular_name'  => 'show debuglog',
        'menu_name' => "show debuglog",
    ],
    'show_in_menu' => 'edit.php?post_type=' . Connection::getWpType(),
    'public' => false,
    'publicly_queryable' => true,
    'exclude_from_search'   =>  true,
    'query_var'             =>  true,
    'has_archive'           =>  false,
    'rewrite'               =>  false,
    'supports'              =>  ['title'],
]);

/**
 * add custom column "shortcode" to posttype "tsCatalog"
 */
add_filter('manage_'. ProductLister::getWpType() .'_posts_columns', function ($column) {
    $column['shortcode'] = 'shortcode';
    return $column;
});

/**
 * add content to custom column "shortcode"
 */
add_filter('manage_'. ProductLister::getWpType() .'_posts_custom_column', function ($column, $id) {
    if ($column == 'shortcode') {
        echo \yii\helpers\Html::textInput('shortcode', '[render_tsCatalog_lister id=' .$id . ']') .
        '<br>' .
        \yii\helpers\Html::textInput('shortcode', '[render_tsCatalog_filter id=' .$id . ']') .
        '<br>' .
        \yii\helpers\Html::textInput('shortcode', '[render_tsCatalog_reader id=' .$id . ']') .
        '<br>' .
        \yii\helpers\Html::textInput('shortcode', '[render_tsCatalog_cart id=' .$id . ']');
    }
}, 10, 2);

/**
 * add Tab-Navigation to backend => post edit
 */
add_action('edit_form_after_title', function (WP_Post $post) {
    if (!Yii::$app->modelRegistry->hasPosttype($post->post_type)) {
        return;
    }

    echo PostTabs::widget();
});

/**
 * On Save a Post, the $_POST-Data are here
 * available.
 */
add_action('save_post', function (int $post_id) {
    $post = get_post($post_id);

    Yii::$app->modelRegistry
        // only process Models attached to post_type
        ->filterPostType($post->post_type)
        // populate (find) data from Database
        ->findByPostId($post_id)
        // fallback for models without post-id (not found in DB)
        // so new instances get an post-id (attribute pid)
        ->setPostId($post_id)
        // populate attributes with new $_POST-Data
        ->load(Yii::$app->request->post())
        // persist without validation:
        // Instead show errors after redirection.
        ->persist(false);
});

/**
 * register the lister shortcode
 * Heads Up: if post/pages are saved in Backend,
 * this would be fired too.
 *
 * Current Implementation checks, if the Accept-requestheaders wants json,
 * if so, no shortcode will be filled.
 */
add_shortcode('render_tsCatalog_lister', function ($atts, $content, $tag) {
    if (!tsCatalog_isFrontend()) {
        return;
    }

    $postId = ArrayHelper::remove($atts, 'id');
    $class = ArrayHelper::remove($atts, 'class');
    $pagerSize = ArrayHelper::remove($atts, 'pager');
    $endpoint = ArrayHelper::remove($atts, 'endpoint');
    $showOnFilter = ArrayHelper::remove($atts, 'showonfilter');
    $themeBlacklist = explode(",", ArrayHelper::remove($atts, 'themeblacklist'));
    $regionBlacklist = explode(",", ArrayHelper::remove($atts, 'regionblacklist'));
    $sort = explode(":", ArrayHelper::remove($atts, 'sort'));
    $filterKey = ArrayHelper::remove($atts, 'filterkey');

    $sortDef = [];
    if (isset($sort[0]) && isset($sort[1])) {
        $sortDef = [$sort[0] => (int)$sort[1]];
    }

    $options = [];
    if ($class) {
        $options['class'] = $class;
    }

    return RenderLister::widget([
        'filterKey' => $filterKey,
        'postId' => $postId,
        'filter' => $atts,
        'options' => $options,
        'pagerSize' => $pagerSize,
        'endpointClassName' => $endpoint,
        'showOnFilter' => (is_string($showOnFilter) ? ($showOnFilter === 'false' ? false : true) : null),
        'themeBlacklist' => $themeBlacklist,
        'regionBlacklist' => $regionBlacklist,
        'sort' => $sortDef
    ]);
});

add_shortcode('render_tsCatalog_filter', function ($atts, $content, $tag) {
    if (!tsCatalog_isFrontend()) {
        return;
    }

    return RenderFilter::widget([
        'postId' => $atts['id']
    ]);
});

add_shortcode('render_tsCatalog_reader', function ($atts, $content, $tag) {
    if (!tsCatalog_isFrontend()) {
        return;
    }

    return RenderReader::widget([
        'postId' => $atts['id']
    ]);
});

add_shortcode('render_tsCatalog_cart', function ($atts, $content, $tag) {
    if (!tsCatalog_isFrontend()) {
        return;
    }

    return RenderCart::widget([
        'postId' => $atts['id']
    ]);
});

/**
 * 1. find all Post->tscatalogconfig === tscatalogconfig
 * 2. load Config (object_id)
 * 3. get themes/locations
 * 4. create WP_Post Objects to parent (menu_item_parent)
 * use Parent as Template (URL)
 */
add_filter(
    'wp_nav_menu_objects',
    function ($sorted_menu_items) {
        $getFilterItemByPost = function ($item, $post, $filter, $isChild=false) {
            $url = $filter->getJumpToPageUrl([
                $filter->filterKey => [$filter->nav_filter_type => $item['id']]
            ]);

            $id = 'ts-catalog-item-' . $item['id'];
            $filterItem = clone $post;
            $filterItem->ID = $id;
            $filterItem->object_id = $id;
            $filterItem->db_id = $id;
            $filterItem->title = $item['title'];
            $filterItem->post_title = $item['title'];
            $filterItem->url = $url;

            if (!$filter->nav_lvl1_as_menuitem || true === $isChild) {
                $filterItem->menu_item_parent = $post->ID;
            }

            return $filterItem;
        };

        foreach ($sorted_menu_items as $key => $post) {
            if (
                $post->object !== 'tscatalogconfig'
                || (!$filter = ProductFilter::findOne(['pid' => $post->object_id]))
            ) {
                continue;
            }

            $config = explode("::config=", $post->title);
            if (isset($config[1])) {
                $post->title = $config[0];
                $post->post_title = $config[0];
                $config = Json::decode($config[1]);

                if (isset($config['type'])) {
                    $filter->nav_filter_type = $config['type'];
                }
            }

            if (isset($config['filter'])) {
                $items = $filter->getNavItems($config['filter']);
            } else {
                $items = $filter->getNavItems();
            }


            foreach ($items as $item) {
                $filterItem = $getFilterItemByPost($item, $post, $filter);

                $sorted_menu_items[] = $filterItem;


                if (isset($item['sub'])) {
                    foreach ($item['sub'] as $subitem) {
                        $objSubItem = $getFilterItemByPost($subitem, $filterItem, $filter, true);
                        $sorted_menu_items[] = $objSubItem;
                    }
                }
            }

            unset($sorted_menu_items[$key]);
        }

        return $sorted_menu_items;
    }
);

/*
 * Some BE-Pages are stored via Ajax (/wp-json/wp/v2/pages) and expect
 * well formed json as result.
 *
 * The WP-Functions is_blog_admin(), is_admin() and wp_doing_ajax() are quirks.
 * @see https://facetwp.com/is_admin-and-ajax-in-wordpress/
 */
function tsCatalog_isFrontend()
{
    $acceptContenttypes = Yii::$app->request->getAcceptableContentTypes();

    if (
        is_blog_admin()
        || is_admin()
        || wp_doing_ajax()
        || isset($acceptContenttypes['application/json'])
    ) {
        return false;
    }

    return true;
}


/**
 * Register Widgets
 */
add_action('widgets_init', function () {
    register_widget(CartWidget::class);
});
